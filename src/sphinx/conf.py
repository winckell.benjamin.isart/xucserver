# -*- coding: utf-8 -*-

# Copyright (C) 2013-2014 Avencall
#
import sys, os

extensions = []
templates_path = [u'_templates']
source_suffix = '.rst'
source_encoding = u'utf-8-sig'
master_doc = u'index'
project = u'Xuc'
copyright = u'2013-2015, Avencall'
version = release = u'2.16.1'
language = u'en'
exclude_patterns = []
pygments_style = 'sphinx'
html_theme = u'nature'
html_static_path = ['_static']
htmlhelp_basename = 'Xuc-docdoc'
latex_paper_size = u'a4'
latex_font_size = u'10pt'
latex_documents = [('index', 'Xuc-doc.tex', u'Xuc-doc Documentation', u'Avencall', 'manual')]
latex_elements = { 'babel': '\\usepackage[english]{babel}' }
latex_use_parts = False
man_pages = [('index', 'Xuc-doc', u'Xuc-doc Documentation', [u'Avencall'], 1)]
epub_title = u'XiVO'
epub_author = u'Avencall'
epub_publisher = u'Avencall'
epub_copyright = u'2011, Avencall'

todo_include_todos = True

pdf_documents = [ ('index', u'MyProject', u'Xuc', u'Avencall') ]
pdf_language = "en_US"
pdf_break_level = 1
pdf_use_toc = True
pdf_toc_depth = 9999
pdf_use_numbered_links = False
pdf_fit_background_mode = 'scale'
