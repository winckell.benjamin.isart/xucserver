#!/bin/sh
mkdir -p /etc/docker/xuc/conf
cd /etc/docker/xuc

mkdir /var/log/xuc
chown daemon:daemon /var/log/xuc

wget https://gitlab.com/xuc/xucserver/raw/master/src/res/update_start.sh
chmod +x update_start.sh

wget https://gitlab.com/xuc/xucserver/raw/master/conf/application.conf
mv application.conf conf/xuc.conf

wget https://gitlab.com/xuc/xucserver/raw/master/conf/xuc_logger.xml
mv xuc_logger.xml conf/

cat > docker-run.conf << EOF
-t
-d
--restart=always
--name=xuc
-p 9000:9000
-v /etc/docker/xuc/conf:/conf/
-v /var/log/xuc:/var/log/xuc/
-v /etc/timezone:/etc/timezone:ro
-v /etc/localtime:/etc/localtime:ro
xivoxc/xuc
-Dconfig.file=/conf/xuc.conf
-Dlogger.file=/conf/xuc_logger.xml
EOF

echo "XUC docker config init done."
echo "Now you have to update the configuration in the /etc/docker/xuc/docker-run.conf and /etc/docker/xuc/conf/xuc.conf."
echo "Than you can start XUC by running 'docker run \$(cat /etc/docker/xuc/docker-run.conf)' or 'update_start.sh' "

