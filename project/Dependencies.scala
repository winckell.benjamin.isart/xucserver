import sbt._
import play.Play.autoImport._

object Version {
  val akka      = "2.3.4"
  val scalaTest = "2.2.4"
  val xivojavactilib = "0.0.33.0"
  val mockito = "1.10.19"
  val xucami = "1.11.2"
  val playauthentication = "2.2"
  val metrics = "3.1.0"
  val dbunit = "2.4.7"
  val scalatestplay = "1.2.0"
  val selenium = "2.31.0"
  val htmlcompressor = "1.4"
  val yuicompressor = "2.4.6"
  val closurecompiler = "r1043"
  val akkaquartz = "0.3.0"
  val metricsPlay = "2.4.0_0.3.0"
  val postgresql = "9.1-901.jdbc4"

}

object Library {
  val xivojavactilib     = "org.xivo"               % "xivo-javactilib"     % Version.xivojavactilib
  val playauthentication = "xivo"                  %% "play-authentication" % Version.playauthentication
  val xucami             = "xivo"                  %% "xucami"              % Version.xucami changing()
  val metrics            = "io.dropwizard.metrics"  % "metrics-core"        % Version.metrics
  val akkaTestkit        = "com.typesafe.akka"     %% "akka-testkit"        % Version.akka
  val scalaTest          = "org.scalatest"         %% "scalatest"           % Version.scalaTest
  val mockito            = "org.mockito"            % "mockito-all"         % Version.mockito
  val dbunit             = "org.dbunit"             % "dbunit"              % Version.dbunit
  val scalatestplay      = "org.scalatestplus"     %% "play"                % Version.scalatestplay
  val selenium        =  "org.seleniumhq.selenium"        %   "selenium-java"       % Version.selenium
  val htmlcompressor  =  "com.googlecode.htmlcompressor"  %   "htmlcompressor"      % Version.htmlcompressor
  val yuicompressor   =  "com.yahoo.platform.yui"         %   "yuicompressor"       % Version.yuicompressor
  val akkaquartz        = "us.theatr"                      %% "akka-quartz"            % Version.akkaquartz
  val metricsPlay       = "com.kenshoo"                    %% "metrics-play"           % Version.metricsPlay
  val postgresql        = "postgresql"                     %  "postgresql"             % Version.postgresql
}

object Dependencies {

  import Library._

  val scalaVersion = "2.11.6"

  val resolutionRepos = Seq(
    ("Local Maven Repository" at "file:///" + Path.userHome.absolutePath + "/.m2/repository"),
    ("theatr.us" at "http://repo.theatr.us"),
    ("Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/")

  )

  val runDep = run(
    xivojavactilib,
    xucami,
    playauthentication,
    javaWs,
    metrics,
    htmlcompressor,
    yuicompressor,
    metricsPlay,
    akkaquartz,
    postgresql,
    jdbc,
    anorm
  )

  val testDep = test(
    scalaTest,
    akkaTestkit,
    mockito,
    dbunit,
    scalatestplay,
    selenium
  )

  def run       (deps: ModuleID*): Seq[ModuleID] = deps
  def test      (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "test")

}
