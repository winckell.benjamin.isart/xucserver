var processDirectoryResult = function(directoryResult) {
    console.log("Directory result" +JSON.stringify(directoryResult));
    DirectoryDisplay.prepareTable();
    DirectoryDisplay.displayHeaders(directoryResult.headers);
    DirectoryDisplay.displayEntries(directoryResult.entries);
};
var linkStatusHandler = function(event) {
    if(event.status === "closed") {
        window.location.replace(jsRoutes.controllers.Pratix.loginError("Erreur de connection au serveur Xuc").url);
        window.location.replace("/?error='unable to connect to xuc server'");
    }  
    if(event.status === "down") {
        window.location.replace(jsRoutes.controllers.Pratix.loginError("Erreur de connection au serveur Cti link down").url);
    }  
};

$("#numberTodial").keypress(function(event) {
    var normalize = function(nb) {
        nb = ""+nb;
        return nb.replace(/[ ()\.]/g,"");
    };
    if ( event.which == $.ui.keyCode.ENTER ) {
        event.preventDefault();
        var toBeDialed = normalize($("#numberTodial").val());
        if ($.isNumeric(toBeDialed)) {
            Cti.dial(toBeDialed);
        }
        else {
            Cti.searchDirectory(toBeDialed);
        }
    }
});
$('#directoryresult').click(function(event){
    console.log($(this).parent().children().text());
    if ($.isNumeric(event.target.firstChild.nodeValue)) {
        Cti.dial(event.target.firstChild.nodeValue);
    }
});
$('#xuc_search').click(function(event){
    Cti.searchDirectory($("#numberTodial").val());
});

Cti.setHandler(Cti.MessageType.DIRECTORYRESULT,processDirectoryResult);
Cti.setHandler(Cti.MessageType.LINKSTATUSUPDATE, linkStatusHandler);

DirectoryDisplay.init("#directoryresult");
var pratixModule = angular.module('Pratix', []);

pratixModule.controller('PhoneController', function($rootScope,$scope, $timeout) {

    $scope.dial = function() {
        Cti.dial($scope.destination);
    };
    $scope.attendedTransfer = function() {
        Cti.attendedTransfer($scope.destination);
    };
    $scope.completeTransfer = function() {
        Cti.completeTransfer();
    };
    $scope.cancelTransfer = function() {
        Cti.cancelTransfer();
    };
    $scope.answer = function() {
        Cti.answer();
    };
    $scope.hangup = function() {
        Cti.hangup();
    };
    $scope.conference = function() {
        Cti.conference();
    };
    $scope.hold = function() {
        Cti.hold();
    };
});

