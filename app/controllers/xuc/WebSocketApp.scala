package controllers.xuc

import com.fasterxml.jackson.databind.JsonNode
import models.{AuthenticatedUser, XivoUser, XucUser}
import play.api.Play.current
import play.api.libs.concurrent.Akka
import play.api.libs.json.JsValue
import play.api.mvc.WebSocket.FrameFormatter
import play.api.mvc.{Action, Controller, WebSocket}
import play.api.{Logger, Routes}
import play.libs.Json
import services.ActorFactory
import services.config.ConfigRepository
import xivo.services.XivoAuthentication
import xivo.websocket.WsActor

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import akka.pattern.ask
import scala.concurrent.duration.DurationInt

object WebSocketApp extends Controller {
  val log = Logger(getClass.getName)

  val stringFrame: FrameFormatter[String] = play.core.server.websocket.Frames.textFrame
  implicit val jsonFrame: FrameFormatter[JsonNode] = stringFrame.transform(Json.stringify, Json.parse)
  implicit val timeout: akka.util.Timeout = 2 seconds

  def javascriptChannel = Action { implicit request =>
    Ok(Routes.javascriptRouter("xucRoutes")(routes.javascript.WebSocketApp.ctiChannel)).as("text/javascript")
  }

  def ctiChannel(username: String, phoneNumber: Int, password: String) = WebSocket.tryAcceptWithActor[JsValue, JsonNode] { implicit request =>
    log.info(s"[$username] websocket requested")
    username match {
      case "" =>
        log.info(s"Cannot create cti websocket username empty from host ${request.remoteAddress}")
        Future.successful(Left(Forbidden("Username cannot be empty")))
      case username =>
        ConfigRepository.repo.getUser(username, password) match {
          case Some(xivoUser) => log.info(s"[$username] internal repo authentication success.")
            WsActor.props(getXucUser(xivoUser, phoneNumber)).map(f => Right(f))

          case None =>
            try {
            AuthenticatedUser.authenticate(username, password) match {
              case Some(user) =>
                log.info(s"[$username] authentication success.")
                ConfigRepository.repo.getUser(username) match {
                  case Some(xivoUser) => WsActor.props(getXucUser(xivoUser, phoneNumber)).map(f => Right(f))

                  case None => Future.successful(Left(Forbidden(s"[$username] Authentication failed, please check your identifiers")))
                }
              case None =>
                log.info(s"[$username] authentication failed.")
                Future.successful(Left(Forbidden(s"[$username] Authentication failed, please check your identifiers")))
            }
        } catch {
          case e: Exception =>
            log.error(s"[$username] Unexpected authentication error ${e.getMessage}")
            Future.successful(Left(InternalServerError(s"[$username] Authentication error, please contact your administrator")))
        }
      }
    }
  }

  def ctiChannelWithToken(token: String) = WebSocket.tryAcceptWithActor[JsValue, JsonNode] { implicit request =>
    log.info(s"Token authentication : [$token] websocket requested")
    token match {
      case "" =>
        log.info(s"Cannot create cti websocket token empty from host ${request.remoteAddress}")
        Future.successful(Left(Forbidden("Token cannot be empty")))
      case _ =>
        val xivoAuth = Akka.system.actorSelection(s"/user/MainRunner/${ActorFactory.XivoAuthenticationId}")
        val userOpt = (xivoAuth ? XivoAuthentication.UserByToken(token)).mapTo[Option[XivoUser]]
        userOpt.flatMap(userOpt => userOpt match {
          case None => Future.successful(Left(Forbidden(s"[$token] Authentication failed, please check your token is valid")))
          case Some(user) => WsActor.props(getXucUser(user, 0)).map(f => Right(f))
        })
    }
  }

  private def getXucUser(xivoUser:XivoUser, phoneNumber: Int) =
    if (phoneNumber == 0)  XucUser(xivoUser.username.getOrElse(""), xivoUser.password.getOrElse(""))
    else  XucUser(xivoUser.username.getOrElse(""), xivoUser.password.getOrElse(""), Some(phoneNumber))
}
