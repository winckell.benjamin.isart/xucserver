package xivo.data

import akka.actor.{ Actor, ActorLogging, ActorRef }
import models.QueueLog.QueueLogData
import models.QueueLog
import stats.Statistic.ResetStat
import scala.concurrent.duration._
import play.api.libs.concurrent.Execution.Implicits._

object QLogTransfer {

  case class LoadQueueLogs(clause: String = QueueLog.defaultClause)
}

class QLogTransfer(qLogGetter: (String) => List[QueueLogData], val qLogDispatcher: ActorRef) extends Actor with ActorLogging {
  import QLogTransfer.LoadQueueLogs

  protected[data] var nextClause = QueueLog.defaultClause

  log.info(s"starting transfert with clause : $nextClause")

  override def preStart(): Unit = {
    qLogDispatcher ! ResetStat
    context.system.scheduler.scheduleOnce(1 second, self, LoadQueueLogs(nextClause))
  }

  def receive = {

    case LoadQueueLogs(clause) =>
      val qLogs = qLogGetter(clause)
      qLogs.foreach(qd => log.debug(s"${qd}"))
      qLogs.foreach(qLogDispatcher ! _)
      if (qLogs.size > 0) nextClause = s"'${qLogs.last.queuetime}'"
      context.system.scheduler.scheduleOnce(1 second, self, LoadQueueLogs(nextClause))

  }

}