package xivo.network

import akka.actor.{Props, Actor}
import play.api.Logger
import org.xivo.cti.MessageFactory
import org.xivo.cti.message.{LoginCapasAck, LoginPassAck, LoginIdAck}
import models.XucUser
import xivo.xuc.XucConfig
import org.xivo.cti.model.UserStatus
object CtiLoginStep {
  def props = Props(new CtiLoginStep(new MessageFactory))
}
class CtiLoginStep(val messageFactory: MessageFactory) extends Actor {
  protected[network] var user: XucUser = null
  val XivoCtiIdentitySuffix = "XuC"
  def identity = user.ctiUsername + XucConfig.XivoCtiIdentitySuffix

  val log = Logger(getClass.getName +"."+ self.path.name)

  def receive = {
    case StartLogin(updatedUser) =>
      user = updatedUser
      log.info(s"Logging on user: ${updatedUser.ctiUsername}")
      val message = messageFactory.createLoginId(user.ctiUsername, identity)
      sender ! message

    case loginIdAck: LoginIdAck =>
      log.debug("Sending loginPass message: " + user.ctiUsername + " sessionId: " + loginIdAck.sesssionId )
      val message = messageFactory.createLoginPass(user.ctiPassword, loginIdAck.sesssionId)
      sender ! message

    case loginPassAck: LoginPassAck =>
      val capaId = loginPassAck.capalist.get(0)
      log.debug("Sending loginCapas message: " + user.ctiUsername + " capaId: " + capaId)
      if (user.phoneNumber != null) {
        val message = messageFactory.createLoginCapas(capaId, user.phoneNumber.toString)
        sender ! message
      }
      else {
        val message = messageFactory.createLoginCapas(capaId)
        sender ! message
      }

    case loginCapasAck: LoginCapasAck =>
      log.debug("User: " + user.ctiUsername + " successfully logged in, loginCapasAck received")
      sender ! LoggedOn(user,loginCapasAck.userId,loginCapasAck.capacities.getUsersStatuses())

    case unknown =>
      log.warn(s"Uknown message received: $unknown from : $sender")

  }
}

case class StartLogin(user: XucUser)
case class LoggedOn(user:XucUser,userId: String,userStatuses: java.util.List[UserStatus])