package xivo.network

import play.api.http.{ContentTypeOf, Writeable}
import play.api.libs.json.JsValue
import play.api.libs.ws._
import xivo.xuc.XucConfig
import play.api.Play.current

trait XiVOWSdef {
  def getWsUrl(resource: String): String
  def withWS(resource: String): WSRequestHolder
  def post(host: String, uri: String, payload: Option[JsValue] = None, user: Option[String] = None,
           password: Option[String] = None, headers: Map[String, String] = Map(),
           port: Option[Int] = None, protocol: String = XucConfig.defaultProtocol): WSRequestHolder
  def genericPost(host: String, uri: String, payload: Option[String] = None, user: Option[String] = None,
           password: Option[String] = None, headers: Map[String, String] = Map(),
           port: Option[Int] = None, protocol: String = XucConfig.defaultProtocol): WSRequestHolder
  def get(host: String, uri: String, payload: Option[JsValue] = None, user: Option[String] = None,
          password: Option[String] = None, headers: Map[String, String] = Map(),
          port: Option[Int] = None, protocol: String = XucConfig.defaultProtocol): WSRequestHolder
  def put(host: String, uri: String, payload: Option[JsValue] = None, user: Option[String] = None,
          password: Option[String] = None, headers: Map[String, String] = Map(),
          port: Option[Int] = None, protocol: String = XucConfig.defaultProtocol): WSRequestHolder
  def del(host: String, uri: String, payload: Option[JsValue] = None, user: Option[String] = None,
          password: Option[String] = None, headers: Map[String, String] = Map(),
          port: Option[Int] = None, protocol: String = XucConfig.defaultProtocol): WSRequestHolder
  }

object XiVOWS extends XiVOWSdef {

  val ReqTimeout = 10000

  def getWsUrl(resource: String) = s"https://${XucConfig.XivoWs_host}:${XucConfig.XivoWs_port}/1.1/$resource"

  def withWS(resource: String): WSRequestHolder = {
    WS.url(getWsUrl(resource))
      .withHeaders(("Content-Type", "application/json"), ("Accept", "application/json"))
      .withAuth(XucConfig.XivoWs_wsUser, XucConfig.XivoWs_wsPwd, WSAuthScheme.DIGEST)
      .withRequestTimeout(ReqTimeout)
  }

  def post(host: String, uri: String, payload: Option[JsValue] = None, user: Option[String] = None,
           password: Option[String] = None, headers: Map[String, String] = Map(),
           port: Option[Int] = None, protocol: String = XucConfig.defaultProtocol): WSRequestHolder = {
    request(host, uri, payload, user, password, headers, port, protocol).withMethod("POST")
  }

  def genericPost(host: String, uri: String, payload: Option[String] = None, user: Option[String] = None,
           password: Option[String] = None, headers: Map[String, String] = Map(),
           port: Option[Int] = None, protocol: String = XucConfig.defaultProtocol): WSRequestHolder = {
    request(host, uri, payload, user, password, headers, port, protocol).withMethod("POST")
  }

  def get(host: String, uri: String, payload: Option[JsValue] = None, user: Option[String] = None,
           password: Option[String] = None, headers: Map[String, String] = Map(),
           port: Option[Int] = None, protocol: String = XucConfig.defaultProtocol): WSRequestHolder = {
    request(host, uri, payload, user, password, headers, port, protocol).withMethod("GET")
  }

  def put(host: String, uri: String, payload: Option[JsValue] = None, user: Option[String] = None,
          password: Option[String] = None, headers: Map[String, String] = Map(),
          port: Option[Int] = None, protocol: String = XucConfig.defaultProtocol): WSRequestHolder = {
    request(host, uri, payload, user, password, headers, port, protocol).withMethod("PUT")
  }

  def del(host: String, uri: String, payload: Option[JsValue] = None, user: Option[String] = None,
          password: Option[String] = None, headers: Map[String, String] = Map(),
          port: Option[Int] = None, protocol: String = XucConfig.defaultProtocol): WSRequestHolder = {
    request(host, uri, payload, user, password, headers, port, protocol).withMethod("DELETE")
  }

  private def request[T](host: String, uri: String, payload: Option[T] = None, user: Option[String] = None,
              password: Option[String] = None, headers: Map[String, String] = Map(),
              port: Option[Int] = None, protocol: String = XucConfig.defaultProtocol)(implicit w: Writeable[T], c: ContentTypeOf[T]): WSRequestHolder = {

    var request = WS.url(s"$protocol://$host${port.fold("")(":"+_)}/$uri")
    headers.foreach( header => request = request.withHeaders(header))
    request = user.fold(request)(request.withAuth(_, password.getOrElse(""), XucConfig.defaultWSAuthScheme))
    payload.fold(request)(request.withBody(_))
  }
}