package xivo.phonedevices

import akka.actor.ActorRef
import services.XucAmiBus.{AmiActionRequest, DialActionRequest, OutBoundDialActionRequest}
import xivo.models.{Agent, Line}
import xivo.xuc.XucConfig

trait RequestToAmi {
  def toAmi(): Option[AmiActionRequest]
}

case class OutboundDial(destination: String, agentId: Agent.Id, queueNumber: String, variables: Map[String, String], xivoHost: String) extends RequestToAmi {

  def toAmi() = Some(OutBoundDialActionRequest(destination,s"select_agent(agent=agent_$agentId)", queueNumber, variables, xivoHost))
}

case class StandardDial(line:Line, caller: String, callee: String, variables: Map[String, String], xivoHost: String) extends RequestToAmi {

  def toAmi() =   Some(DialActionRequest(s"${line.protocol}/${line.name}", line.context, caller, callee, variables, xivoHost))
}

trait DeviceAdapter {

  def dial(destination: String, variables: Map[String, String], sender: ActorRef)
  def answer(sender: ActorRef)
  def hangup(sender: ActorRef)
  def attendedTransfer(destination: String, sender: ActorRef)
  def completeTransfer(sender: ActorRef)
  def cancelTransfer(sender: ActorRef)
  def conference(sender: ActorRef)
  def hold(sender: ActorRef)

  def odial(destination: String, agentId: Agent.Id, queueNumber: String, variables: Map[String, String], sender: ActorRef) = {
    if (destination.length > XucConfig.outboundLength && queueNumber != "") {
      sender ! OutboundDial(destination, agentId, queueNumber, variables, XucConfig.xivoHost)
    }
    else {
      dial(destination, variables, sender)
    }
  }
}