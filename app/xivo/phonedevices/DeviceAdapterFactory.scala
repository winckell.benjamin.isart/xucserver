package xivo.phonedevices

import xivo.models.Line

class DeviceAdapterFactory {

  def getAdapter(line: Line, lineNumber: String): DeviceAdapter = lineAdapter(line).getOrElse(defaultAdapter(line, lineNumber))

  private def lineAdapter(line: Line): Option[DeviceAdapter] =
    (for {
      device <- line.dev
      ip <- device.ip
    } yield vendorAdapter(device.vendor,ip)).flatten

  private def vendorAdapter(vendor : String, ip : String): Option[DeviceAdapter] = vendor match {
    case "Snom" =>   Some(new SnomDevice(ip))
    case _ => None
  }

  private def defaultAdapter(line:Line, lineNumber: String) = new CtiDevice(line, lineNumber)
}