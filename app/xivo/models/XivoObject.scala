package xivo.models

object XivoObject {

  object ObjectType extends Enumeration {
    type ObjectType = Value
    val Queue = Value
  }

  import ObjectType._
  case class ObjectDefinition( objectType : ObjectType, objectRef : Option[Int] = None) {
    def hasObjectRef: Boolean = (objectRef != None)
  }
}