package xivo.models

import anorm.SqlParser._
import anorm._
import org.xivo.cti.message.AgentConfigUpdate
import play.api.Play.current
import play.api.db.DB
import play.api.libs.functional.syntax._
import play.api.libs.json.Writes._
import play.api.libs.json._


case class Agent(val id: Agent.Id, val firstName: String, val lastName: String, val number: String, val context: String, val groupId : Long = 0, val userId : Long = 0)

trait AgentFactory  {
  def getById(id: Agent.Id) : Option[Agent]
  def moveAgentToGroup(agentId: Long, groupId: Long)
}

object Agent extends AgentFactory {
  type Id = Long
  type Number = String


  val simple = {
      get[Long]("id") ~
      get[String]("firstname") ~
      get[String]("lastname") ~
      get[String]("number") ~
      get[String]("context") ~
      get[Long]("numgroup")  ~
      get[Long]("userid") map {
      case id ~ firstname ~ lastname ~ number ~ context ~ groupId ~ userId =>
        Agent(id, firstname, lastname, number, context, groupId, userId)
    }
  }

  implicit val agWrites = (
    (__ \ "id").write[Long] and
    (__ \ "firstName").write[String] and
    (__ \ "lastName").write[String] and
    (__ \ "number").write[String] and
    (__ \ "context").write[String] and
    (__ \ "groupId").write[Long] and
    (__ \ "userId").write[Long])(unlift(Agent.unapply))


  def apply(agentConfig: AgentConfigUpdate) =
    new Agent(agentConfig.getId().toLong, agentConfig.getFirstName(), agentConfig.getLastName(), agentConfig.getNumber(), agentConfig.getContext())

  def getById(id: Agent.Id): Option[Agent] = {
    DB.withConnection { implicit c =>
      SQL(
        """select a.id,
          |       a.firstname as firstname,
          |       a.lastname as lastname,
          |       a.number as number,
          |       a.context as context,
          |       a.numgroup as numgroup,
          |       u.id as userid
          |from agentfeatures a inner join userfeatures u on u.agentid=a.id
          |where a.id={id} order by u.id limit 1""".stripMargin)
        .on('id -> id)
        .as(simple *).headOption
    }

  }

  override def moveAgentToGroup(agentId: Id, groupId: Id): Unit = DB.withConnection(implicit c =>
    SQL("UPDATE agentfeatures SET numgroup={groupId} WHERE id = {agentId}").on('groupId -> groupId, 'agentId -> agentId)
    .executeUpdate())
}