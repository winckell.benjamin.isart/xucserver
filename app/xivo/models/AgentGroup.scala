package xivo.models

import anorm.SqlParser._
import anorm._
import play.api.db.DB
import play.api.Play.current
import play.api.libs.functional.syntax._
import play.api.libs.json._


case class AgentGroup(id: Option[Long], name: String, displayName: Option[String]=None)


trait AgentGroupFactory {
  def all(): List[AgentGroup]
}

object AgentGroup extends AgentGroupFactory {

  implicit val agGroupWrites = (
    (__ \ "id").write[Option[Long]] and
    (__ \ "name").write[String] and
    (__ \ "displayName").write[Option[String]])(unlift(AgentGroup.unapply))

  val simple = get[Long]("id") ~ get[String]("name") ~ get[Option[String]]("description") map {
      case id ~ name ~ displayName => AgentGroup(Some(id), name, displayName)
    }


  override def all(): List[AgentGroup] =
    DB.withConnection { implicit c =>
      SQL("select  id, name, description from agentgroup")
        .as(simple *)
    }

  def toJson(lagentGroup : List[AgentGroup]) = {
    Json.toJson(lagentGroup)
  }
}