package xivo.events

import play.api.libs.json._
import xivo.events.PhoneEventType.PhoneEventType


object PhoneEventType extends Enumeration {
  type PhoneEventType = Value
  val EventReleased, EventDialing, EventRinging, EventEstablished = Value

  implicit def enumWrites: Writes[PhoneEventType.PhoneEventType] = new Writes[PhoneEventType.PhoneEventType] {
    def writes(value : PhoneEventType.PhoneEventType): JsValue = JsString(value.toString)
  }

}

object UserData {
  val QueueNameKey = "XIVO_QUEUENAME"
  type userData = Map[String,String]
  type filter = (userData) => userData
  val incPrefix = "USR_"
  val includeKeys = List("XIVO_USERID","XIVO_SRCNUM","XIVO_CONTEXT","XIVO_EXTENPATTERN","XIVO_DSTNUM","XIVO_REVERSE_LOOKUP","XUC_CALLTYPE","XIVO_DST_FIRSTNAME","XIVO_DST_LASTNAME")
  def filterData(variables:userData):userData = variables.filterKeys(key => includeKeys.contains(key)) ++ variables.filterKeys(_.startsWith(incPrefix))
}

object PhoneEvent {
  implicit val writes :Writes[PhoneEvent] =  Json.writes[PhoneEvent]

}

case class PhoneEvent(eventType: PhoneEventType,  DN: String, otherDN: String, linkedId: String, uniqueId: String, queueName:Option[String] = None, userData: Map[String,String] = Map())

