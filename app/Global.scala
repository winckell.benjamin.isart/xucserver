import models.authentication.AuthenticationProvider
import org.slf4j.MDC
import play.api.Application
import play.api.GlobalSettings
import play.api.Logger
import play.api.libs.concurrent.Akka
import play.api.Play.current
import system.MainRunner
import xivo.xuc.XucConfig
import xucserver.info.BuildInfo

object Global extends GlobalSettings {

  override def onStart(app: Application) {
    MDC.put("version",BuildInfo.version)

    Logger.info(s"Starting xuc version ${BuildInfo.version}")
    Logger.info("-------------parameters--------------------")
    Logger.info(s"host           : ${XucConfig.XIVOCTI_HOST}")
    Logger.info(s"port           : ${XucConfig.XIVOCTI_PORT}")
    Logger.info(s"wsHost         : ${XucConfig.XivoWs_host}")
    Logger.info(s"wsUser         : ${XucConfig.XivoWs_wsUser}")
    Logger.info(s"peers          : ${XucConfig.xucPeers}")
    Logger.info(s"outboundLength : ${XucConfig.outboundLength}")
    Logger.info(s"valid authent  : ${AuthenticationProvider.validAuthentications.length > 0}")

    if (AuthenticationProvider.validAuthentications.length == 0)
      Logger.warn("No authentication configured")

    if (XucConfig.EventUser!= "") startMainRunner
    else Logger.error("xivocti.eventUser missing unable to start")
  }

  override def onStop(app: Application) {
    Logger.debug("Shutting down actor system")
  }

  def startMainRunner() {
    val runner = Akka.system.actorOf(MainRunner.props,"MainRunner")
  }

}
