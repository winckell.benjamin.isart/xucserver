package services

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import play.api.libs.json.{JsError, JsSuccess}
import services.config.ConfigRepository
import services.request.{AgentCallHistoryRequest, BaseRequest, UserCallHistoryRequest}
import xivo.models.CallHistory
import xivo.network._
import xivo.websocket.WsBus.WsContent
import xivo.websocket.{WSMsgType, WebSocketEvent}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object CallHistoryManager {
  def props(recordingWs: RecordingWS) = Props(new CallHistoryManager(recordingWs))
}

class CallHistoryManager(ws: RecordingWS) extends Actor with ActorLogging {

  override def receive: Receive = {
    case BaseRequest(ref, AgentCallHistoryRequest(size, ctiUserName)) => log.debug(s"Received agent call history request for user $ctiUserName")
      ConfigRepository.repo.agentFromUsername(ctiUserName).foreach(agent => getCallHistory(ref, size, agent.number, ws.getCallHistory, 1))

    case BaseRequest(ref, UserCallHistoryRequest(size, ctiUserName)) => log.debug(s"Received user call history request for user $ctiUserName")
      ConfigRepository.repo.interfaceFromUsername(ctiUserName).foreach(
        interface => getCallHistory(ref, size, interface, ws.getUserCallHistory, 1))
  }

  def getCallHistory(ref: ActorRef, size: Int, term: String, searchFunction: (Int, String) => Future[HistoryServerResponse], nbRetries: Int): Unit = {
    log.debug(s"Searching call history with term $term")
    searchFunction(size, term).map({
      case HistoryServerResponse(json) => CallHistory.validate(json) match {
        case JsSuccess(history, _) => ref ! history
        case JsError(e) => log.error(s"Error decoding the json: $e")
      }
    }).onFailure({case t => fail(ref, s"Connection to history server failed with message ${t.getMessage}")})
  }

  def fail(ref: ActorRef, message: String): Unit = {
    log.error(message)
    ref ! WsContent(WebSocketEvent.createError(WSMsgType.Error, message))
  }
}
