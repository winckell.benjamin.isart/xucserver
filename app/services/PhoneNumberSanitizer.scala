package services

/**
  * Created by fvoron on 10/06/16.
  * Simple helper class to sanitize phone number strings
  * coming from web applications or APIs (like Zimbra connector)
  */
object PhoneNumberSanitizer {

  val patterns = List(
    /* Remove whitespaces */
    ("\\s+", ""),
    /* Remove dots */
    ("\\.+", ""),
    /* Remove dashes */
    ("\\-+", ""),
    /* Remove slashes */
    ("/+", ""),
    /* Handle plus-prefixed country code (with 2 or 3 digits) */
    ("^\\+(\\d{2,3})", "00$1"),
    /* Remove zero between parenthesis */
    ("\\(0\\)", ""),
    /* Remove non-digits characters */
    ("\\D+", "")
  )

  def sanitize(phoneNumber: String): String = {
    def _sanitize(phoneNumber: String, patterns: List[(String, String)]): String = {
      if (patterns.isEmpty) phoneNumber
      else _sanitize(patterns.head._1.r.replaceAllIn(phoneNumber, patterns.head._2), patterns.tail)
    }
    _sanitize(phoneNumber, patterns)
  }
}
