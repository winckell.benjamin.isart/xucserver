package services

import akka.actor.{ActorRef, ActorContext}
import services.agent.AgentStatCollector
import xivo.models.Agent

import scala.collection.mutable.{ Map => MMap }

class AgentActorFactory() {
    val agFsms: MMap[Agent.Id, ActorRef] = MMap()
    val agStatCollectors: MMap[Agent.Id, ActorRef] = MMap()

    private def create(id: Agent.Id, context : ActorContext): ActorRef = context.actorOf(AgentStateFSM.props,s"AgentStateFSM$id")
    private def createStatCollector(id: Agent.Id, agentFSM: ActorRef, context : ActorContext): ActorRef = context.actorOf(AgentStatCollector.props(id, agentFSM),s"AgentStatCollector$id")

    def getOrCreate(id: Agent.Id, context : ActorContext): ActorRef = {
        agFsms get id match {
            case Some(actorRef) => actorRef
            case None =>
                agFsms += (id -> create(id, context))
                agFsms(id)
        }
    }

    def getOrCreateAgentStatCollector(id: Agent.Id, context: ActorContext): ActorRef = {
        agStatCollectors get id match {
            case Some(actorRef) => actorRef
            case None =>
                val agentFSM = getOrCreate(id, context)
                agStatCollectors += (id -> createStatCollector(id, agentFSM, context))
                agStatCollectors(id)
        }
    }
    def get(id: Agent.Id): Option[ActorRef] = agFsms.get(id)
}