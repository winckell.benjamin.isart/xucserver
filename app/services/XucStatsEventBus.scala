package services

import akka.event.ActorEventBus
import akka.event.SubchannelClassification
import akka.util.Subclassification
import play.api.libs.json.{JsValue, Writes}
import stats.Statistic.RequestStat
import xivo.models.XivoObject.ObjectDefinition
import akka.actor.ActorRef
import play.api.Logger
import play.api.libs.functional.syntax._
import play.api.libs.json._

object XucStatsEventBus {
  trait StatEvent {
    val xivoObject: ObjectDefinition
    val stat: List[Stat]
  }
  case class Stat(statRef: String, value: Double)
  object Stat {
    implicit val statWrites = (
      (__ \ "statName").write[String] and
        (__ \ "value").write[Double])(unlift(Stat.unapply)
      )
  }
  case class AggregatedStatEvent(override val xivoObject: ObjectDefinition, override val stat: List[Stat]) extends StatEvent
  case class StatUpdate(override val xivoObject: ObjectDefinition, override val stat: List[Stat]) extends StatEvent

  object AggregatedStatEvent {
    implicit val AggregatedStatEventWrites =  (
        (__ \ "queueId").write[Long] and
          (__ \ "counters").write[List[Stat]])(unlift(AggregatedStatEvent.extract)
    )

    def extract(ags : AggregatedStatEvent):Option[(Long,List[Stat])] = Some((ags.xivoObject.objectRef.get.toLong, ags.stat))
  }

  val statEventBus = new XucStatsEventBus
}

class XucStatsEventBus extends ActorEventBus with SubchannelClassification {
  import XucStatsEventBus._
  type Event = XucStatsEventBus.StatEvent
  type Classifier = ObjectDefinition

  var busManager:Option[ActorRef] = None

  def setBusManager(busManager:ActorRef) = this.busManager = Some(busManager) 

  protected def subclassification = new Subclassification[Classifier] {
    def isEqual(x: Classifier, y: Classifier) = x == y
    def isSubclass(x: Classifier, y: Classifier) = (x.objectType == y.objectType && (!y.hasObjectRef))
  }

  override def classify(event: Event) = event.xivoObject

  protected def publish(event: Event, subscriber: Subscriber) = subscriber ! event

  override def subscribe(subscriber: Subscriber, to: Classifier) = {
    Logger.debug(s"subscribe : $subscriber to : $to")
    busManager.foreach(_ ! RequestStat(subscriber,to))
    super.subscribe(subscriber, to)
  }
}