package services

import akka.actor.{Actor, ActorLogging, Props}
import services.config.UserPhoneStatus

object StatusPublish {
  def props = Props(new StatusPublish())
}
class StatusPublish extends Actor with ActorLogging{

  log.info("StatusPublish started " + self)

  def receive = {

    case userPhoneStatus: UserPhoneStatus =>
      log.debug("received : " + userPhoneStatus)
      StatusPublisher.publishStatus(userPhoneStatus)

    case unknown => log.debug("Uknown message received: ")
  }

}