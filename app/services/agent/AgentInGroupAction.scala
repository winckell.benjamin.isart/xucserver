package services.agent

import akka.actor.{PoisonPill, ActorLogging, Actor}
import services.ProductionActorFactory
import services.agent.AgentInGroupAction.{RemoveAgents, AgentsDestination}
import services.config.ConfigDispatcher.{AgentList, GetAgents, RequestConfig}
import xivo.models.Agent
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import scala.concurrent.duration.DurationInt

object AgentInGroupAction {
  case class AgentsDestination(queueId : Long, penalty : Int)
  case object RemoveAgents
}
abstract class AgentInGroupAction(groupId: Long, fromQueueId : Long, fromPenalty: Int) extends  Actor with ActorLogging with ProductionActorFactory {

  val RequestTimeOut = 1 second

  context.system.scheduler.scheduleOnce(RequestTimeOut) {
    self ! PoisonPill
  }

  override def receive: Receive = {

    case AgentsDestination(toQueueId, toPenalty) =>
      context.actorSelection(configDispatcherURI) !  RequestConfig(self, GetAgents(groupId, fromQueueId, fromPenalty))
      context.become(forAllAgents(Some(toQueueId), Some(toPenalty)))

    case RemoveAgents =>
      context.actorSelection(configDispatcherURI) !  RequestConfig(self, GetAgents(groupId, fromQueueId, fromPenalty))
      context.become(forAllAgents())


    case unkown => log.debug(s"unkown $unkown message received")
  }

  def forAllAgents(toQueueId : Option[Long] = None, toPenalty : Option[Int]= None) : Receive = {

    case AgentList(agents) =>
      agents.foreach(actionOnAgent(_, toQueueId, toPenalty))
      self ! PoisonPill

    case unkown => log.debug(s" action unkown $unkown message received")

  }


  def actionOnAgent(agent: Agent, toQueueId: Option[Long], toPenalty: Option[Int])

  override def postStop() = log.debug(s"Move in group $groupId $fromQueueId $fromPenalty stopped")
}
