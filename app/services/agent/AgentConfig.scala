package services.agent

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import models.{QueueMembership, UserIdList, UserQueueDefaultMembership, UsersQueueMembership}
import services.ProductionActorFactory
import services.agent.AgentInGroupAction.{AgentsDestination, RemoveAgents}
import services.config.ConfigDispatcher.{ConfigChangeRequest, GetAgentByUserId, GetQueuesForAgent, QueuesForAgent, RemoveAgentFromQueue, RequestConfig}
import services.config.ConfigServerRequester
import services.request._
import xivo.models.{Agent, AgentQueueMember}
import xivo.network.XiVOWS

import scala.concurrent.ExecutionContext.Implicits.global

object AgentConfig {
  def props = Props(new AgentConfig(new ConfigServerRequester(XiVOWS)))

  // Internal messages
  private[AgentConfig] sealed trait AgentConfigInternalRequest
  private[AgentConfig] case class ApplyDefaultMembership(membership: UserQueueDefaultMembership) extends AgentConfigInternalRequest
  private[AgentConfig] case class ApplyAgentDefaultMembership(agentId: Agent.Id, membership: UserQueueDefaultMembership) extends AgentConfigInternalRequest
}

class AgentConfig(configRequester: ConfigServerRequester) extends Actor with ActorLogging with ProductionActorFactory {

  import AgentConfig._

  var pendingDefaultRequest: Map[Long, UserQueueDefaultMembership] = Map.empty
  var agentMap: Map[Agent.Id, Long] = Map.empty

  override def receive: Receive = {

    case BaseRequest(requester, rq: AgentConfigRequest) => handleRequest(requester).apply(rq)

    case r:AgentConfig.AgentConfigInternalRequest => handleInternal(r)

    case a:Agent =>
      agentMap = agentMap + (a.id -> a.userId)
      context.actorSelection(configDispatcherURI) ! RequestConfig(self, GetQueuesForAgent(a.id))

    case QueuesForAgent(activeQueues, agentId) =>
      agentMap.get(agentId)
        .flatMap(pendingDefaultRequest.get(_))
        .map(d =>  applyDefaultConfiguration(agentId, activeQueues, d.membership))

    case unknown => log.debug(s"unkown $unknown message received")
  }

  def handleInternal: PartialFunction[AgentConfigInternalRequest, Unit] = {
    case ApplyDefaultMembership(membership) =>
      pendingDefaultRequest = pendingDefaultRequest + (membership.userId -> membership)
      context.actorSelection(configDispatcherURI) ! RequestConfig(self, GetAgentByUserId(membership.userId))

    case ApplyAgentDefaultMembership(agentId, membership) =>
  }

  def handleRequest(requester: ActorRef): PartialFunction[AgentConfigRequest, Unit]  = {
    case MoveAgentInGroup(groupId,fromQueueId,fromPenalty,toQueueId,toPenalty) =>
      getMoveAgentInGroup(groupId, fromQueueId, fromPenalty) ! AgentsDestination(toQueueId, toPenalty)

    case AddAgentInGroup(groupId,fromQueueId,fromPenalty,toQueueId,toPenalty) =>
      getAddAgentInGroup(groupId, fromQueueId, fromPenalty) ! AgentsDestination(toQueueId, toPenalty)

    case AddAgentsNotInQueueFromGroupTo(groupId, toQueueId, toPenalty) =>
      getAddAgentsNotInQueueFromGroupTo(groupId, toQueueId, toPenalty) ! AgentsDestination(toQueueId, toPenalty)

    case RemoveAgentGroupFromQueueGroup(groupId,queueId,penalty) =>
      getRemoveAgentGroupFromQueueGroup(groupId, queueId, penalty) ! RemoveAgents

    case SetAgentGroup(gId, aId) =>
      getAgentGroupSetter() ! SetAgentGroup(gId, aId)

    case SetUserDefaultMembership(userId, membership) =>
      configRequester.setUserDefaultMembership(userId, membership)

    case SetUsersDefaultMembership(userIds, membership) =>
      configRequester.setUsersDefaultMembership(UsersQueueMembership(userIds, membership))

    case GetUserDefaultMembership(userId) =>
      configRequester.getUserDefaultMembership(userId).map(m => requester ! UserQueueDefaultMembership(userId, m))

    case ApplyUsersDefaultMembership(userIds) =>
      configRequester.findUsersDefaultMembership(UserIdList(userIds)).map(l => l.foreach(self ! ApplyDefaultMembership(_)))
  }

  private def applyDefaultConfiguration(agentId: Agent.Id, active: List[AgentQueueMember], default: List[QueueMembership]): Unit = {
    val activeMap:Map[Long, Int] = active.map(q => q.queueId -> q.penalty)(collection.breakOut)
    val defaultMap:Map[Long, Int] = default.map(q => q.queueId -> q.penalty)(collection.breakOut)

    for(q <- active if !defaultMap.contains(q.queueId)) {
      context.actorSelection(configDispatcherURI) ! ConfigChangeRequest(self, RemoveAgentFromQueue(agentId, q.queueId))
    }

    default.foreach(defQ => {
      activeMap.get(defQ.queueId) match {
        case Some(actPenalty) =>
          if (defQ.penalty != actPenalty)
            context.actorSelection(configDispatcherURI) ! ConfigChangeRequest(self, SetAgentQueue(agentId, defQ.queueId, defQ.penalty))

        case None =>
          context.actorSelection(configDispatcherURI) ! ConfigChangeRequest(self, SetAgentQueue(agentId, defQ.queueId, defQ.penalty))
      }
    })

  }
}
