package services.agent

import akka.actor.{Actor, Props}
import services.{ProductionActorFactory, ActorFactory}
import services.request.SetAgentGroup
import xivo.models.{RefreshAgent, Agent, AgentFactory}

object AgentGroupSetter {
  def props() = Props(new AgentGroupSetter(Agent) with ProductionActorFactory)
}

class AgentGroupSetter(agent: AgentFactory) extends Actor with ActorFactory {
  override def receive: Receive = {
    case SetAgentGroup(agentId, groupId) => agent.moveAgentToGroup(agentId, groupId)
      context.actorSelection(configDispatcherURI) ! RefreshAgent(agentId)
  }
}
