package services.agent

import services.{XucEventBus, AgentStateFSM}
import services.AgentStateFSM._
import xivo.events.AgentState
import xivo.events.AgentState.AgentOnCall

trait AgentStateTHandler {
  this: AgentStateFSM =>

  val lineEventSubs:Lsubs
  val eventPublisher: EventPublisher
  val transitionPublisher: TransitionPublisher

  class Lsubs {
    def subscribe :TransitionHandler = {
      case _ -> MAgentLoggedOut => eventBus.unsubscribe(self)

      case MAgentLoggedOut -> _ => doSubscribe

      case MInitState -> _      => doSubscribe

    }
    private def doSubscribe = nextStateData match {
      case agc:AgentStateContext =>  subscribeToLineEvent(agc.phoneNumber)
      case _ =>
    }

    private def subscribeToLineEvent(line: String): Unit = {
      def toInt(s: String): Option[Int] = {
        try {
          Some(s.toInt)
        } catch {
          case e: NumberFormatException =>  None
        }
      }
      toInt(line).map { nb =>
        log.debug(s"subscribing to line event $nb")
        eventBus.subscribe(self, XucEventBus.lineEventTopic(nb))
      }
    }
  }

  class TransitionPublisher {
    def publish : TransitionHandler = {
      case from -> to =>
        nextStateData match {
          case a: AgentStateContext =>
            eventBus.publish(AgentTransition(from, stateData, to, nextStateData), a.state.agentId)
          case _ =>
        }
    }
  }

  class EventPublisher {
    def publish: TransitionHandler = {
      case MAgentDialing -> MAgentDialingOnPause => log.debug(s"Not publishing from MAgentDialing, to MAgentDialingOnPause")
      case MAgentDialingOnPause -> MAgentDialing => log.debug(s"Not publishing from MAgentDialingOnPause, to MAgentDialing")
      case MAgentRinging -> MAgentRingingOnPause => log.debug(s"Not publishing from MAgentRinging, to MAgentRingingOnPause")
      case MAgentRingingOnPause -> MAgentRinging => log.debug(s"Not publishing from MAgentRingingOnPause, to MAgentRinging")
      case MAgentOnCall -> MAgentOnCallOnPause => log.debug(s"Not publishing from MAgentOnCall, to MAgentOnCallOnPause")
      case MAgentOnCallOnPause -> MAgentOnCall => log.debug(s"Not publishing from MAgentOnCallOnPause, to MAgentOnCall")
      case from -> to => sendBusHandler(from,to)
    }
    private def sendBusHandler(from: MAgentStates, to: MAgentStates) {
      nextStateData match {
        case nextState: AgentStateContext =>
            log.debug(s"Transition from: '$from', to: '$to', publishing agentStatus: $nextState")
            publishState(nextState)

        case _ =>
          log.debug(s"Transition from: '$from', to: '$to', not publishing agentStatus due to missing status in the context")
      }
    }

    private def publishState(nextContext: AgentStateContext): Unit = {
      nextContext.state withCause nextContext.userStatus withPhoneNb(nextContext.phoneNumber) match {
        case onCall : AgentOnCall =>
          eventBus.publish(onCall withMonitorState nextContext.monitorState)
        case agentState: AgentState =>
          eventBus.publish(agentState)
        case _ =>
      }
    }
  }
}
trait AgentStateProdTHandler extends AgentStateTHandler {
  this: AgentStateFSM =>

  override lazy val lineEventSubs:Lsubs = new Lsubs()
  override lazy val eventPublisher: EventPublisher = new EventPublisher()
  override lazy val transitionPublisher: TransitionPublisher = new TransitionPublisher()
}
