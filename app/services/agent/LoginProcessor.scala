package services.agent

import akka.actor._
import services.request.{AgentLoginRequest, BaseRequest}
import services.{ProductionActorFactory, ActorFactory, XucEventBus}
import xivo.events.AgentLoginError
import xivo.events.AgentState.{AgentOnCall, AgentLogin, AgentReady}
import xivo.models.Agent.Id
import xivo.xuc.api.{RequestError, RequestResult, RequestSuccess, RequestTimeout}

import scala.concurrent.duration._

object LoginProcessor {
  def props(): Props =  Props(new LoginProcessor() with ProductionActorFactory)
}

class LoginProcessor(val eventBus: XucEventBus = XucEventBus.bus)
  extends Actor with ActorLogging {
  this: ActorFactory =>

  log.info(s"starting $self")

  context.setReceiveTimeout(60 seconds)

  override def receive: Receive = {
    case req: AgentLoginRequest =>
      context.actorSelection(configManagerURI) ! BaseRequest(self, req)
      context.setReceiveTimeout(1 seconds)
      context.become(expectingResult(sender, req))

    case ReceiveTimeout =>
      log.error(s"LoginProcessor started and not used, stopping itself")
      context.stop(self)

    case unknown =>
      log.error(s"Should not happen, in receive received: $unknown")
      context.stop(self)
  }

  def expectingResult(requester: ActorRef, req: AgentLoginRequest): Receive = {

    case AgentLogin(id, _, phoneNb, _, _) =>
      log.debug(s"Agent id: $id logged on $phoneNb")
      requester ! RequestSuccess(s"Agent id: $id logged on $phoneNb")
      context.stop(self)

    case AgentLoginError(error) =>
      requester ! RequestError(s"AgentLogin error: $error")
      context.stop(self)

    case r: RequestResult =>
      requester ! r
      context.stop(self)

    case ReceiveTimeout =>
      log.error(s"Agent id: ${req.id.getOrElse("UNKNOWN")} login on phone: ${req.phoneNumber.getOrElse("UNKNOWN")} timed out")
      requester ! RequestTimeout(s"Agent id: ${req.id.getOrElse("UNKNOWN")} login on phone: ${req.phoneNumber.getOrElse("UNKNOWN")} timed out")
      context.stop(self)

    case unknown =>
      log.debug(s"Dropped, in expectingResult received: $unknown")
  }

}


