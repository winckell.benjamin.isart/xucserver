package services.request

import xivo.models.Agent
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, JsValue, Reads}

class UpdateConfigRequest extends XucRequest

case class SetAgentQueue(agentId : Agent.Id, queueId : Long, penalty : Int) extends UpdateConfigRequest
object SetAgentQueue {
  def validate(json: JsValue) = json.validate[SetAgentQueue]
  implicit val SetAgentQueueReads: Reads[SetAgentQueue] = (
    (JsPath \ "agentId").read[Agent.Id] and
      (JsPath \ "queueId").read[Long] and
      (JsPath \ "penalty").read[Int]
    )(SetAgentQueue.apply _)
}

