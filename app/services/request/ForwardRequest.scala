package services.request

import play.api.libs.json.{JsValue, JsPath, Reads}
import play.api.libs.functional.syntax._

abstract class ForwardRequest(destination: String, state: Boolean) extends XucRequest

object ForwardRequest {

  val fwdPath = (JsPath \ "destination").read[String] and (JsPath \ "state").read[Boolean]

}

case class NaForward(destination: String, state: Boolean) extends ForwardRequest(destination, state)
case class UncForward(destination: String, state: Boolean) extends ForwardRequest(destination, state)
case class BusyForward(destination: String, state: Boolean) extends ForwardRequest(destination, state)

object NaForward {
  def validate(json: JsValue) = json.validate[NaForward]
  implicit val NaForwardRead :Reads[NaForward] = (ForwardRequest.fwdPath) (NaForward.apply _ )
}

object UncForward {
  def validate(json: JsValue) = json.validate[UncForward]
  implicit val UncForwardRead :Reads[UncForward] = (ForwardRequest.fwdPath) (UncForward.apply _ )
}

object BusyForward {
  def validate(json: JsValue) = json.validate[BusyForward]
  implicit val BusyForwardRead :Reads[BusyForward] = (ForwardRequest.fwdPath) (BusyForward.apply _ )
}