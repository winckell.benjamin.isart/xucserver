package services.request

import akka.actor.ActorRef
import models.XucUser

class XucRequest
object XucRequest {
  val ClassProp = "claz"
  val PingClass = "ping"
  val WebClass = "web"
  val Cmd = "command"

  object errors {
    val invalid = "Invalid request"
    val invalidNoCmd = s"$invalid no cmd field"
    val validation = "Validation error"
  }
}

object Ping extends XucRequest

case class InvalidRequest(reason: String, request: String) extends  XucRequest

case class BaseRequest(requester: ActorRef, request : XucRequest)
case class UserBaseRequest(requester: ActorRef, request : XucRequest, user: XucUser)
