package services.request

import java.util.UUID

import models.CallbackStatus
import play.api.libs.json._
import play.api.libs.functional.syntax._

case object GetCallbackLists extends XucRequest {
  def validate(json: JsValue) = JsSuccess(GetCallbackLists)
}

case class TakeCallback(uuid: String) extends XucRequest

case class TakeCallbackWithAgent(uuid: String, userName: String) extends XucRequest

object TakeCallback {
  implicit val reads = Json.reads[TakeCallback]
  def validate(json: JsValue) = json.validate[TakeCallback]
}

case class CallbackTaken(uuid: UUID, agentId: Long)

object CallbackTaken {
  implicit val writes =  new Writes[CallbackTaken] {
    def writes(cb: CallbackTaken) = Json.obj(
      "uuid" -> cb.uuid,
      "agentId" -> cb.agentId
    )
  }
}

case class ReleaseCallback(uuid: String) extends XucRequest

object ReleaseCallback {
  implicit val reads = Json.reads[ReleaseCallback]
  def validate(json: JsValue) = json.validate[ReleaseCallback]
}

case class CallbackReleased(uuid: UUID)

object CallbackReleased {
  implicit val writes =  new Writes[CallbackReleased] {
    def writes(cb: CallbackReleased) = Json.obj(
      "uuid" -> cb.uuid
    )
  }
}

case class StartCallback(uuid: String, number: String) extends XucRequest

object StartCallback {
  implicit val reads = Json.reads[StartCallback]
  def validate(json: JsValue) = json.validate[StartCallback]
}

case class StartCallbackWithUser(uuid: String, number: String, username: String) extends XucRequest

case class CallbackStarted(requestUiid: UUID, ticketUuid: UUID)

object CallbackStarted {
  implicit val writes =  new Writes[CallbackStarted] {
    def writes(cb: CallbackStarted) = Json.obj(
      "requestUuid" -> cb.requestUiid,
      "ticketUuid" -> cb.ticketUuid
    )
  }
}

case class UpdateCallbackTicket(uuid: String, status: Option[CallbackStatus.CallbackStatus], comment: Option[String]) extends XucRequest

object UpdateCallbackTicket {
  implicit val reads: Reads[UpdateCallbackTicket] = (
    (JsPath \ "uuid").read[String] and
    (JsPath \ "status").readNullable[String].map(_.map(s => CallbackStatus.withName(s.toLowerCase))) and
    (JsPath \ "comment").readNullable[String]
  )(UpdateCallbackTicket.apply _)

  def validate(json: JsValue) = json.validate[UpdateCallbackTicket]
}

case class CallbackClotured(uuid: UUID)

object CallbackClotured {
  implicit val writes =  new Writes[CallbackClotured] {
    def writes(cb: CallbackClotured) = Json.obj("uuid" -> cb.uuid)
  }
}