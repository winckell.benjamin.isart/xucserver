package services.request

import play.api.libs.json.{JsPath, Reads, JsValue}

case class GetUserCallHistory(size: Int) extends XucRequest
case class UserCallHistoryRequest(size: Int, ctiUserName: String) extends XucRequest

object GetUserCallHistory {
  def validate(json: JsValue) = json.validate[GetUserCallHistory]

  implicit val reads: Reads[GetUserCallHistory] = (JsPath \ "size").read[Int].map(GetUserCallHistory.apply)
}
