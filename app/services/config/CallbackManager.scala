package services.config

import java.util.UUID

import akka.actor.{Actor, ActorLogging, Props}
import models.{CallbackStatus, CallbackLists, CallbackTicket, CallbackTicketPatch}
import services.request.PhoneRequest.Dial
import services.request._
import services.{ActorFactory, ProductionActorFactory, XucEventBus}
import xivo.events.{PhoneEventType, PhoneEvent}
import xivo.network.RecordingWS
import xivo.websocket.WsBus.WsContent
import xivo.websocket.{WSMsgType, WebSocketEvent}
import xivo.xuc.api.{RequestError, RequestSuccess}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

object CallbackManager {
  def props(requester: ConfigServerRequester, recordingWS: RecordingWS) = Props(new CallbackManager(requester, recordingWS) with ProductionActorFactory)
}

case class ImportCsvCallback(listUuid: String, csv: String)
case class ExportTicketsCsv(listUuid: String)

class CallbackManager(configRequester: ConfigServerRequester, recordingWS: RecordingWS, bus: XucEventBus = XucEventBus.bus) extends Actor with ActorLogging {
  this: ActorFactory =>

  bus.subscribe(self, XucEventBus.allPhoneEventsTopic)

  override def receive: Receive = {
    case BaseRequest(ref, GetCallbackLists) => configRequester.getCallbackLists.onComplete({
      case Success(list) => ref ! CallbackLists(list)
      case Failure(exception) => ref ! WsContent(WebSocketEvent.createError(WSMsgType.Error, exception.getMessage))
    })

    case ImportCsvCallback(uuid, csv) => val theSender = sender
      configRequester.importCsvCallback(uuid, csv).onComplete({
      case Success(_) => configRequester.getCallbackLists.foreach(bus.publish)
        theSender ! RequestSuccess("CSV successfully imported")
      case Failure(e) => theSender ! RequestError(e.getMessage)
    })

    case TakeCallbackWithAgent(uuid, userName) => ConfigRepository.repo.agentFromUsername(userName).foreach(agent =>
      configRequester.takeCallback(uuid, agent).onComplete({
        case Success(_) => bus.publish(CallbackTaken(UUID.fromString(uuid), agent.id))
        case Failure(e) => log.error(s"Error taking the callback request $uuid with agent $agent", e)
      }))

    case ReleaseCallback(uuid) => configRequester.releaseCallback(uuid).onComplete({
      case Success(_) => bus.publish(CallbackReleased(UUID.fromString(uuid)))
      case Failure(e) => log.error(s"Error releasing the callback request $uuid", e)
    })

    case BaseRequest(ref, StartCallbackWithUser(requestUuid, number, username)) =>
      ConfigRepository.repo.agentFromUsername(username) match {
        case None => log.error(s"$username has no agent, cannot take a callback")
        case Some(agent) => configRequester.getCallbackRequest(requestUuid)
          .foreach(rq => recordingWS.createCallbackTicket(CallbackTicket.fromRequest(rq, agent.number))
            .onComplete({
              case Success(ticket) =>
                context.actorSelection(getCtiRouterUri(username)) ! BaseRequest(ref, Dial(number, Map("CALLBACK_TICKET_UUID" -> ticket.uuid.get.toString)))
                ref ! CallbackStarted(ticket.requestUuid, ticket.uuid.get)
              case Failure(e) => log.error("exception" + e.getMessage)
            })
          )
      }

    case BaseRequest(ref, UpdateCallbackTicket(uuid, status, comment)) => recordingWS.updateCallbackTicket(uuid, CallbackTicketPatch(status, comment, None))
      status match {
        case Some(CallbackStatus.Callback) => recordingWS.getCallbackTicket(uuid).onComplete({
          case Success(t) => configRequester.unclotureRequest(t.requestUuid)
          case Failure(e) => log.error(s"Cannot retrieve ticket $uuid", e)
        })
        case Some(_) => recordingWS.getCallbackTicket(uuid).onComplete({
          case Success(t) => configRequester.clotureRequest(t.requestUuid).onSuccess({
            case _ => bus.publish(CallbackClotured(t.requestUuid))
          })
          case Failure(e) => log.error(s"Cannot retrieve ticket $uuid", e)
        })
        case _ =>
      }

    case ExportTicketsCsv(listUuid) => val theSender = sender
      recordingWS.exportTicketsCSV(listUuid).onComplete({
        case Success(result) => theSender ! RequestSuccess(result)
        case Failure(e) => theSender ! RequestError(e.getMessage)
      })

    case PhoneEvent(PhoneEventType.EventDialing, _, _, linkedId, _, _, variables) if variables.contains("USR_CALLBACK_TICKET_UUID") =>
      val ticketUuid = variables("USR_CALLBACK_TICKET_UUID")
      recordingWS.updateCallbackTicket(ticketUuid, CallbackTicketPatch(None, None, Some(linkedId)))
      .onFailure({
        case e: Exception => log.error(s"Exception when setting the callid or ticket $ticketUuid", e)
      })
  }
}