package services.config

import akka.actor.{ActorLogging, ActorRef}
import models.XivoUserDao
import org.joda.time.DateTime
import org.json.JSONObject
import org.xivo.cti.message._
import org.xivo.cti.{CtiMessage, MessageFactory}
import play.api.libs.concurrent.Execution.Implicits._
import services.XucEventBus
import services.XucEventBus.XucEvent
import services.config.ConfigDispatcher.ObjectType
import services.config.ConfigInitializer.CheckRequests
import xivo.events.AgentState.{AgentLoggedOut, AgentLogin}
import xivo.models.{AgentLoginStatusDao, Agent}
import scala.collection.JavaConversions._

import scala.concurrent.duration.DurationInt
import scala.util.{Failure, Success}

object ConfigInitializer {
  case object CheckRequests
  case class ConfigRequest(request: JSONObject)
}

trait ConfigInitializer {
  this: ConfigDispatcher with ActorLogging with XivoUserDao with AgentLoginStatusDao =>

  import services.config.ConfigInitializer.ConfigRequest
  import services.config.ConfigManager.InitConfig

  protected[config] var messageFactory = new MessageFactory

  protected[config] var requestsToSend: List[JSONObject] =
    List(
      messageFactory.createGetPhonesList(),
      messageFactory.createGetQueues(),
      messageFactory.createGetUsersList(),
      messageFactory.createSubscribeToQueueStats(),
      messageFactory.createSubscribeToMeetmeUpdate())
  protected[config] var link: ActorRef = _
  protected[config] var requests = requestsToSend

  def configInitializerReceive: Receive = {

    case InitConfig(ctiLink) =>
      log.info(s"Configuration initialization requested using $ctiLink")
      initUsers
      link = ctiLink
      requests = requestsToSend
      processRequests
      configRepository.loadAgentQueueMembers()

    case CheckRequests => if (requests.size > 0) processRequests

    case ConfigRequest(request) =>
      log.debug(s"will send : ${request}")
      link ! request
      processRequests

    case agent: Agent =>
      log.debug(s"$agent")
      getLoginStatus(agent.id) match {
        case Some(agLStatus) =>
          log.debug(s"agent $agent.id already logged in $agLStatus")
          agentManager ! AgentLogin(agLStatus)
        case None => agentManager ! AgentLoggedOut(agent.id, new DateTime(),"", List())
      }
      link ! messageFactory.createGetAgentStatus(agent.id.toString)
      configRepository.getAgentQueueMemberRequest(agent.number).foreach(link ! _)
      eventBus.publish(XucEvent(XucEventBus.configTopic(ObjectType.TypeAgent),agent))

    case message: CtiMessage => {
      message match {
        case message: IdsList => {
          val newRequests = message match {
            case phoneIds: PhoneIdsList => configRepository.onPhoneIds(phoneIds)
            case userIds: UserIdsList => configRepository.onUserIds(userIds)
            case queueIds: QueueIds => configRepository.onQueueIds(queueIds)
            case queueMemberIds: QueueMemberIds => configRepository.onQueueMemberIds(queueMemberIds)
            case _ => List()
          }
          log.debug("new requests = " + newRequests)
          requests = requests ++ newRequests
        }

        case queueConfig: QueueConfigUpdate => configRepository.updateQueueConfig(queueConfig)

        case userConfig: UserConfigUpdate =>
          log.debug(s"$userConfig")
          updateUser(userConfig)
          userConfig.getLineIds.toList.headOption.map(lineId => configRepository.loadUserLine(userConfig.getUserId.toLong, lineId.toLong))
          requests = messageFactory.createGetUserStatus(userConfig.getUserId().toString) :: requests

        case queueMember: QueueMemberConfigUpdate =>
          val agqm = agentQueueMemberFactory.getQueueMember(queueMember.getAgentNumber(), queueMember.getQueueName())
          agqm match {
            case Some(agentQMember) => self ! agentQMember
            case None => log.error(s"Config : Unable to get queue member from db for $queueMember")
          }
        case queueMember: QueueMemberAdded =>
          val agqm = agentQueueMemberFactory.getQueueMember(queueMember.getAgentNumber(), queueMember.getQueueName())
          agqm match {
            case Some(agentQMember) => self ! agentQMember
            case None => log.error(s"Config : Unable to get queue member from db for $queueMember")
          }

        case qmRemoved: QueueMemberRemoved =>
          configRepository.removeQueueMember(qmRemoved)
          configRepository.getAgentQueueMember(qmRemoved).foreach(agqm => eventBus.publish(XucEvent(XucEventBus.configTopic(ObjectType.TypeQueueMember),agqm)))

        case m: CtiMessage => configDispatcherReceive(m)
      }
    }
  }

  private def processRequests = {

    requests match {
      case List() =>
        log.info("Pending requests done, checking for new requests")
        checkPendingRequests
      case toProcess =>
        self ! ConfigRequest(toProcess.head)
        requests = toProcess.tail
    }
  }

  private def checkPendingRequests {
    context.system.scheduler.scheduleOnce(2 second) {
      self ! CheckRequests
    }
  }

  private def initUsers() =  {
    val fget = getUsers()
    fget.onComplete{
      case Success(users) =>
        log.debug(s"initializing users $users")
        users.map(user => configRepository.updateUser(user))
      case Failure(ex) => log.error(ex, "unable to retreive list of users using web services")
    }
  }

  private def updateUser(userConfig: UserConfigUpdate): Unit = {
    updateUsername(userConfig.getUserId)
    configRepository.onUserConfigUpdate(userConfig)
    updateAgent(userConfig.getAgentId)
  }

  private def updateAgent(agentId: Agent.Id): Unit = {
    if(agentId != 0) {
      configRepository.loadAgent(agentId)
      configRepository.getAgent(agentId) match {
        case Some(agent) => self ! agent
        case None => log.error(s"Config : Cannot find any agent in database for $agentId")
      }
    }

  }
  private def updateUsername(userId: Long) = {
    val fget = getUser(userId)
    fget.onComplete{
      case Success(Some(user)) =>
        log.debug(s"updatijng $user")
        configRepository.updateUser(user)
      case Failure(ex) => log.error(ex, "unable to retreive list of users using web services")
      case Success(None) => log.error(s"user $userId not found")
    }
  }


}