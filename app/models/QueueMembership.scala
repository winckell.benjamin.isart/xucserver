package models

import play.api.libs.json._
import play.api.libs.functional.syntax._

case class QueueMembership(queueId: Long, penalty: Int)

object QueueMembership {
  implicit val queueMembershipWrites = new Writes[QueueMembership] {
    override def writes(o: QueueMembership): JsValue = Json.obj(
      "queueId" -> o.queueId,
      "penalty" -> o.penalty
    )
  }

  implicit val queueMembershipReads: Reads[QueueMembership] = (
    (JsPath \ "queueId").read[Long] and
      (JsPath \ "penalty").read[Int]
    )(QueueMembership.apply _)
}

case class UserQueueDefaultMembership(userId: Long, membership: List[QueueMembership])

object UserQueueDefaultMembership {
  implicit val userQueueMembershipWrites = new Writes[UserQueueDefaultMembership] {
    override def writes(o: UserQueueDefaultMembership): JsValue = Json.obj(
      "userId" -> o.userId,
      "membership" -> o.membership
    )
  }

  implicit val userQueueMembershipReads: Reads[UserQueueDefaultMembership] = (
    (JsPath \ "userId").read[Long] and
      (JsPath \ "membership").read[List[QueueMembership]]
    )(UserQueueDefaultMembership.apply _)
}