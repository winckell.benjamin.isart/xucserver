package models

import anorm._
import anorm.SqlParser.get
import play.api.Logger
import play.api.db.DB
import play.api.Play.current
import xivo.xucstats.XucStatsConfig

object QueueLog {
  val log = Logger(getClass.getName)
  val defaultClause1Mn = "cast((now() - interval '1 minutes') as varchar)"
  val defaultClauseMidnight = "cast(current_date as varchar)"

  def defaultClause = if (XucStatsConfig.initFromMidnight) defaultClauseMidnight else  defaultClause1Mn

  class ObjectEvent
  case object Unknown extends ObjectEvent
  case class EnterQueue(queueId: Int) extends ObjectEvent
  case class Abandonned(queueId: Int, waitTime: Int) extends ObjectEvent
  case class Complete(queueId: Int, agentNumber: String, waitTime: Int, callDuration: Int) extends ObjectEvent
  case class Connect(queueId: Int, agentNumber: String, waitTime: Int, callId: String) extends ObjectEvent
  case class Closed(queueId: Int) extends ObjectEvent
  case class Timeout(qeuueId: Int, waitTime: Int) extends ObjectEvent

  case class QueueLogData(queuetime: String, agent: String, event: String,
                          data1: Option[String] = None, data2: Option[String] = None,
                          data3: Option[String] = None, queueid: Option[Int] = None)


  def query(clause: String) =s"""
          select
              ql.time as queuetime,
              ql.agent,
              ql.event,
              ql.data1,
              ql.data2,
              ql.data3,
              qf.id as queueid,
              ql.queuename,
              qf.name
          from queue_log as ql
          left outer join queuefeatures as qf on (ql.queuename = qf.name)
          where ql.time > $clause
          order by ql.time
    """

  val simple = {
      get[String]("queuetime") ~
      get[String]("agent") ~
      get[String]("event") ~
      get[Option[String]]("data1") ~
      get[Option[String]]("data2") ~
      get[Option[String]]("data3") ~
      get[Option[Int]]("queueid") map {
      case queuetime ~ agent ~ event ~ data1 ~ data2 ~ data3 ~ queueid =>
        QueueLogData(queuetime, agent, event, data1, data2, data3, queueid)
    }
  }

  def getAll(clause: String = defaultClause): List[QueueLogData] = {
    DB.withConnection { implicit c =>
      log.debug(s"fetching $clause of queue log")
      SQL(query(clause)).as(simple *)
    }
  }
}