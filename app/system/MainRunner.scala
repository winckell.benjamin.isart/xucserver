package system

import akka.actor.{Actor, ActorLogging, Props, Terminated}
import com.codahale.metrics.jvm.{MemoryUsageGaugeSet, ThreadStatesGaugeSet}
import com.codahale.metrics.{SharedMetricRegistries, Slf4jReporter}
import com.kenshoo.play.metrics.MetricsRegistry
import models.{QLogTransformer, QueueLog, XucUser}
import play.api.libs.concurrent.Akka
import play.api.{Logger, Mode, Play}
import services._
import services.agent.{AgentConfig, AgentDeviceManager}
import services.config._
import services.directory.{DirectoryTransformer, DirectoryTransformerLogic}
import services.phone.PhoneEventTransformer
import stats.StatsAggregator.SubscribeToBus
import stats.{StatsAggregator, QueueStatCache}
import us.theatr.akka.quartz.QuartzActor
import xivo.ami.AmiBusConnector
import xivo.data.{QLogDispatcher, QLogTransfer}
import xivo.network.{RecordingWS, XiVOWS}
import xivo.services.{XivoAuthentication, XivoDirectory}
import xivo.xuc.{XucStatsConfig, XucConfig}
import java.util.concurrent.TimeUnit
import javax.crypto.Cipher
import scala.collection.JavaConversions._

object MainRunner {
  case class StartUser(user:XucUser)

  val MainRunnerPath = s"/user/${ActorFactory.MainRunnerId}"

  def props = Props(new MainRunner() with ProductionActorFactory)
}

class MainRunner extends Actor with ActorLogging {
  this: ActorFactory =>

  log.info(s"Xuc Main runner started ................$self")

  logCipherKeyLength()

  if (log.isDebugEnabled) { logSystemProperties() }

  startStatistics()

  if (XucConfig.EventUser!= "") startAllActors()

  startTechMetrics()

  def receive = {
    case Terminated(actor) =>
      log.error(s"$actor stopped")
    case _ =>
  }

  private def startAllActors() = {
    val publisher = context.actorOf(StatusPublish.props, ActorFactory.StatusPublishId)
    context.watch(publisher)
    val agentManager = getAgentManager
    val lineManager = getLineManager
    val configRepository = ConfigRepository.repo
    val configDispatcher = context.actorOf(ConfigDispatcher.props(configRepository, lineManager, agentManager), ActorFactory.ConfigDispatcherId)
    context.actorOf(AmiBusConnector.props(configRepository, agentManager, lineManager, configDispatcher), ActorFactory.AmiBusConnectorId)
    context.watch(configDispatcher)
    val configManager = context.actorOf(ConfigManager.props(XucUser(XucConfig.EventUser, XucConfig.DefaultPassword), agentManager), ActorFactory.ConfigManagerId)
    context.watch(configManager)
    context.watch(context.actorOf(AgentConfig.props,ActorFactory.AgentConfigId))
    context.actorOf(AgentDeviceManager.props, ActorFactory.AgentDeviceManagerId)
    val recordingWs = new RecordingWS(XucConfig.recordingHost, XucConfig.recordingPort, XucConfig.recordingToken)
    context.actorOf(CallHistoryManager.props(recordingWs), ActorFactory.CallHistoryManagerId)
    val xivoAuthentication = context.actorOf(XivoAuthentication.props(configRepository, XiVOWS), ActorFactory.XivoAuthenticationId)
    val directoryTransformer = context.actorOf(DirectoryTransformer.props(new DirectoryTransformerLogic(configRepository)))
    context.actorOf(XivoDirectory.props(xivoAuthentication, directoryTransformer,
      configRepository, XiVOWS), ActorFactory.XivoDirectoryInterfaceId)
    if(XucConfig.configHost.isEmpty || XucConfig.configPort == 0 || XucConfig.configToken.isEmpty) {
      log.warning("Rights management server not totally configured. CallbackManager not launched.")
    } else {
      context.actorOf(CallbackManager.props(new ConfigServerRequester(XiVOWS), recordingWs), ActorFactory.CallbackMgrInterfaceId)
    }
    context.actorOf(PhoneEventTransformer.props, ActorFactory.PhoneEventTransformerId)
  }

  private def startTechMetrics() {
    if (Play.current.mode == Mode.Prod) {
      val registry = SharedMetricRegistries.getOrCreate(XucConfig.metricsRegistryName)
      if (XucConfig.metricsRegistryJVM) {
        log.info("Activating JVM metrics")
        registry.registerAll(new MemoryUsageGaugeSet())
        registry.registerAll(new ThreadStatesGaugeSet())
      }

      if (XucConfig.metricsLogReporter) {
        log.info(s"Activating logReporter with period: ${XucConfig.metricsLogReporterPeriod} minutes")
        val reporter = Slf4jReporter.forRegistry(registry)
          .outputTo(Logger.logger)
          .convertRatesTo(TimeUnit.SECONDS)
          .convertDurationsTo(TimeUnit.MILLISECONDS)
          .build()
        reporter.start(XucConfig.metricsLogReporterPeriod, TimeUnit.MINUTES)
      }
    }
  }

  private def logCipherKeyLength() {
    try {
      val allowedKeyLength = Cipher.getMaxAllowedKeyLength("AES")
      log.info("The allowed key length for AES is: " + allowedKeyLength)
    } catch {
      case e: Throwable =>
        log.error("Unable to get max cipher length")
        log.error(e.getStackTrace.mkString("\n"))
    }
  }

  private def logSystemProperties() {
    val environmentVars = System.getenv
    for ((k,v) <- environmentVars) log.debug(s"envVar '$k': $v")

    val properties = System.getProperties
    for ((k,v) <- properties) log.debug(s"propsKey '$k': $v")
  }

  private def startStatistics() {
    Logger.info("Starting xuc stats")
    Logger.info("-------------parameters--------------------")
    Logger.info(s"resetSchedule           : ${XucStatsConfig.resetSchedule}")
    Logger.info(s"initFromMidnight        : ${XucStatsConfig.initFromMidnight}")
    Logger.info(s"statsLogReporter        : ${XucStatsConfig.statsLogReporter}")
    Logger.info(s"statsLogReporterPeriod  : ${XucStatsConfig.statsLogReporterPeriod}")

    val cronScheduler = context.actorOf(Props[QuartzActor])
    val qlogDispatcher = context.actorOf(QLogDispatcher.props(QLogTransformer.transform), "QLogDispatcher")
    val qStatCache = context.actorOf(QueueStatCache.props,"QStatCache")
    val agregator = initiateGlobalAggregator
    XucStatsEventBus.statEventBus.setBusManager(qStatCache)
    val qlogTransfer = context.actorOf(Props(new QLogTransfer(QueueLog.getAll, qlogDispatcher)), "QLogTransfer")

    if (XucStatsConfig.statsLogReporter) {
      val reporter= Slf4jReporter.forRegistry(MetricsRegistry.defaultRegistry)
        .outputTo(Logger.logger)
        .convertRatesTo(TimeUnit.SECONDS)
        .convertDurationsTo(TimeUnit.MILLISECONDS)
        .build()
      reporter.start(XucStatsConfig.statsLogReporterPeriod, TimeUnit.MINUTES)
    }
  }

  private def initiateGlobalAggregator = {
    val globalAggregator = context.actorOf(StatsAggregator.props, "GlobalAggregator")
    globalAggregator ! SubscribeToBus
    globalAggregator
  }
}
