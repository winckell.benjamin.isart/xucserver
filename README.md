# Xuc

XiVOcc / XiVOuc integration server

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

See the [COPYING](COPYING) and [COPYING.LESSER](COPYING.LESSER) files for details.

## Documentation

    http://xivocc.readthedocs.org

## How to develop :

Starting the application in play framework :

run -Dlogger.file=conf/debug-logger.xml  -DtechMetrics.jvm=false -Dhttp.port=8090 -Dxivohost=192.168.56.3

Using xucami as a dependency :
- In Build.scala Uncomment
    .dependsOn(xucami)
    .aggregate(xucami)
    

- In Dependencies.scala
    Comment xucami dependency


##Miscellaneous

- Close xuc port

    iptables -A INPUT -p tcp --dport 9000 -j DROP

- Open xuc port

    iptables -D INPUT -p tcp --dport 9000 -j DROP


## Testing

In /etc/hosts, map the IP of an existing XiVO to the hostname xivo-integration. Ex:
192.168.51.232 xivo-integration

## Unit tests

### Setup a test database *testdata* user *test* pwd *test*

    CREATE USER test WITH PASSWORD 'test';
    CREATE DATABASE testdata WITH OWNER test;

In activator start test

## Integration Tests

* Have access to a xivo with a configured webservice user *xivows* password *xivows* host *your_host_ip*
* In file /etc/hosts add xivo-integration pointing to xivo ip address
* In activator console : integrationTest:test
* activator test wsapi:testOnly


## Notes

Some test output with no effect : 

    java.lang.NullPointerException
        at akka.actor.ActorLogging$class.log(Actor.scala:286)

### Kamon

use specific kamon branch

http://kamon.io

docker run -d -p 8000:80 -p 8125:8125/udp -p 8126:8126 --name kamon-grafana-dashboard kamon/grafana_graphite

activator stage
cd target/universal/stage/
./bin/xuc -J-javaagent:./lib/org.aspectj.aspectjweaver-1.8.6.jar -Dlogger.file=conf/debug-logger.xml
    -DtechMetrics.jvm=false -Dhttp.port=8090 -Dxivohost=192.168.51.232 -Dxuc.outboundLength=4 -Dconfig.file=./conf/application.conf
