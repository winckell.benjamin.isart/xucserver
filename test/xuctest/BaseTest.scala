package xuctest

import org.scalatest.{Tag, Matchers, WordSpec}

object IntegrationTest extends Tag("xuc.tags.IntegrationTest")


class BaseTest extends WordSpec with Matchers {
  def xivoIntegrationConfig: Map[String, String] = {
    Map(
      "XivoWs.wsUser" -> "xivows",
      "XivoWs.wsPwd" -> "xivows",
      "XivoWs.host" -> "xivo-integration",
      "XivoWs.port" -> "9486",
      "ws.acceptAnyCertificate" -> "true"
    )
  }

}
