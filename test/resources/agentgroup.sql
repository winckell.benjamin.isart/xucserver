drop table if exists agentgroup;

CREATE TABLE agentgroup
(
  id serial NOT NULL,
  groupid integer NOT NULL,
  name character varying(128) NOT NULL DEFAULT ''::character varying,
  groups character varying(255) NOT NULL DEFAULT ''::character varying,
  commented integer NOT NULL DEFAULT 0,
  deleted integer NOT NULL DEFAULT 0,
  description text,
  CONSTRAINT agentgroup_pkey PRIMARY KEY (id)
)