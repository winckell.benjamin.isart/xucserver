CREATE TYPE generic_bsfilter AS ENUM (
    'no',
    'boss',
    'secretary'
);

CREATE TYPE userfeatures_voicemailtype AS ENUM (
    'asterisk',
    'exchange'
);

CREATE TABLE userfeatures (
    id integer NOT NULL,
    uuid character varying(38) NOT NULL,
    firstname character varying(128) DEFAULT ''::character varying NOT NULL,
    lastname character varying(128) DEFAULT ''::character varying NOT NULL,
    voicemailtype userfeatures_voicemailtype,
    voicemailid integer,
    agentid integer,
    pictureid integer,
    entityid integer,
    callerid character varying(160),
    ringseconds integer DEFAULT 30 NOT NULL,
    simultcalls integer DEFAULT 5 NOT NULL,
    enableclient integer DEFAULT 0 NOT NULL,
    loginclient character varying(64) DEFAULT ''::character varying NOT NULL,
    passwdclient character varying(64) DEFAULT ''::character varying NOT NULL,
    cti_profile_id integer,
    enablehint integer DEFAULT 1 NOT NULL,
    enablevoicemail integer DEFAULT 0 NOT NULL,
    enablexfer integer DEFAULT 1 NOT NULL,
    enableautomon integer DEFAULT 0 NOT NULL,
    callrecord integer DEFAULT 0 NOT NULL,
    incallfilter integer DEFAULT 0 NOT NULL,
    enablednd integer DEFAULT 0 NOT NULL,
    enableunc integer DEFAULT 0 NOT NULL,
    destunc character varying(128) DEFAULT ''::character varying NOT NULL,
    enablerna integer DEFAULT 0 NOT NULL,
    destrna character varying(128) DEFAULT ''::character varying NOT NULL,
    enablebusy integer DEFAULT 0 NOT NULL,
    destbusy character varying(128) DEFAULT ''::character varying NOT NULL,
    musiconhold character varying(128) DEFAULT ''::character varying NOT NULL,
    outcallerid character varying(80) DEFAULT ''::character varying NOT NULL,
    mobilephonenumber character varying(128) DEFAULT ''::character varying NOT NULL,
    userfield character varying(128) DEFAULT ''::character varying NOT NULL,
    bsfilter generic_bsfilter DEFAULT 'no'::generic_bsfilter NOT NULL,
    preprocess_subroutine character varying(39),
    timezone character varying(128),
    language character varying(20),
    ringintern character varying(64),
    ringextern character varying(64),
    ringgroup character varying(64),
    ringforward character varying(64),
    rightcallcode character varying(16),
    commented integer DEFAULT 0 NOT NULL,
    description text NOT NULL,
    func_key_template_id integer,
    func_key_private_template_id integer NOT NULL
);


CREATE SEQUENCE userfeatures_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


ALTER SEQUENCE userfeatures_id_seq OWNED BY userfeatures.id;

ALTER TABLE ONLY userfeatures ALTER COLUMN id SET DEFAULT nextval('userfeatures_id_seq'::regclass);
