package xivo.phonedevices

import akka.actor.ActorRef
import akka.testkit.TestProbe
import akkatest.TestKitSpec
import org.mockito.Mockito.verify
import org.scalatest.mock.MockitoSugar
import play.api.test.FakeApplication
import play.api.test.Helpers._
import services.XucAmiBus.DialActionRequest
import xivo.models.Line

class DeviceAdapterSpec extends TestKitSpec("deviceAdapter") with MockitoSugar {

  class Helper(val outboundLength:Int) {

    val mockDial = mock[(String, Map[String, String], ActorRef)=>Unit]

    class Device extends DeviceAdapter {
      override def dial(destination: String, variables: Map[String, String], sender: ActorRef): Unit = mockDial(destination, variables, sender)
      override def conference(sender: ActorRef): Unit = {}
      override def attendedTransfer(destination: String, sender: ActorRef): Unit = {}
      override def completeTransfer(sender: ActorRef): Unit = {}
      override def hold(sender: ActorRef): Unit = {}
      override def hangup(sender: ActorRef): Unit = {}
      override def cancelTransfer(sender: ActorRef): Unit = {}
      override def answer(sender: ActorRef): Unit = {}
    }

    val sender = TestProbe()
    val outDevice = new Device()

    val xivoHost = "10.20.30.50"

    val fApp = FakeApplication(additionalConfiguration = Map("xuc.outboundLength" -> outboundLength, "xivohost" -> xivoHost))
  }

  "a device adapter" should {
    "return an outbound call if number length > configured length and outboutqueue configured" in new Helper(6) {
      val destination = "01234567"
      val agentId = 58
      val queueNumber = "3000"
      val variables = Map("VAR" -> "value")
      running(fApp) {
        outDevice.odial(destination, agentId, queueNumber, variables, sender.ref)
        sender.expectMsg(OutboundDial(destination, agentId, queueNumber, variables, xivoHost))
      }
    }

    "call adapter dial if destination length < configured length and outboutqueue configured" in new Helper(5) {
      val destination = "1250"
      val queueNumber = "3000"
      val variables = Map("VAR" -> "value")

      running(fApp) {
        outDevice.odial(destination, 58, queueNumber, variables, sender.ref)
        verify(mockDial)(destination, variables, sender.ref)
      }

    }
    "call adapter dial if destination length > configured length and outboutqueue not configured" in new Helper(6) {
      val destination = "12504567987"
      val queueNumber = ""
      val variables = Map("VAR" -> "value")

      running(fApp) {
        outDevice.odial(destination, 58, queueNumber, variables, sender.ref)

        verify(mockDial)(destination, variables, sender.ref)
      }
    }
  }

  "StandardDial" should {
    "return DialActionRequestion when the requested line exists" in {
      val line = Line(1, "default", "SIP", "callerLine", None, None, "ip")

      val sd = StandardDial(line, "1000", "0230210000", Map("VAR" -> "Value"), "xivoHost")

      sd.toAmi shouldBe Some(DialActionRequest("SIP/callerLine", "default", "1000", "0230210000", Map("VAR" -> "Value"), "xivoHost"))
    }

  }
}
