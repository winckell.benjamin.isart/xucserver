package xivo.data.dbunit

import anorm._
import org.dbunit.database.{DatabaseConfig, DatabaseConnection}
import org.dbunit.dataset.xml.{FlatXmlDataSet, FlatXmlProducer}
import org.dbunit.ext.postgresql.PostgresqlDataTypeFactory
import org.dbunit.operation.DatabaseOperation
import org.xml.sax.InputSource
import play.api.Play.current
import play.api.db.DB

object DBUtil {
  private var dbunitConnection: DatabaseConnection = null

  val factory = new PostgresqlDataTypeFactory(){
    override def isEnumType(sqlTypeName : String):Boolean = {
      return true
    }
  }

  def setupDB(fileName: String) = {
    DB.withConnection { implicit conn =>

      var dataset = new FlatXmlDataSet(new FlatXmlProducer(new InputSource(inputFromFile(fileName))))

      try {
        this.dbunitConnection = new DatabaseConnection(conn)
        this.dbunitConnection.getConfig().setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, factory)


        for (table <- dataset.getTableNames()) {
          val createTable = scala.io.Source.fromInputStream(inputFromFile(s"${table}.sql")).mkString
          SQL(createTable).execute
        }
      } catch {
        case e: Exception => println(s"****SetupDB Create table Error : $e")
      }
      try {
      DatabaseOperation.CLEAN_INSERT.execute(this.dbunitConnection, dataset)
      } catch {
        case e: Exception => println(s"****SetupDB insert data Error : $e")
      }
    }
  }
  private def inputFromFile(fileName:String) = getClass().getClassLoader().getResourceAsStream(fileName)
}