package xivo.data

import akka.testkit.{ TestProbe, TestActorRef }
import org.mockito.Mockito._
import org.scalatest.mock.MockitoSugar
import models.QLogTransformer.QLogDataTransformer
import models.QueueLog._
import stats.Statistic.ResetStat
import xivo.models.XivoObject.ObjectDefinition
import akkatest.TestKitSpec

class QLogDispatcherSpec extends TestKitSpec("QLogDispatcherSpec")
  with MockitoSugar {

  class Helper {
    val qLogTransformer = mock[QLogDataTransformer]
    val eventObjQueueDispatcher = TestProbe()

    trait TestEvtDispatcher extends EvtDispatcher {
      val queueEventDispatcher = eventObjQueueDispatcher.ref
    }

    def actor() = {
      val a = TestActorRef(new QLogDispatcher(qLogTransformer) with TestEvtDispatcher)
      (a, a.underlyingActor)
    }
  }

  "Qlog dispatcher" should {
    "forward reset event " in new Helper {
      val (ref, _) = actor()

      ref ! ResetStat

      eventObjQueueDispatcher.expectMsg(ResetStat)

    }
    "transform qlog data to qlog event" in new Helper {
      val (ref, _) = actor()
      val qLogData = QueueLogData("2014-04-18 12:22:20.225112", "Agent/1702", "WRAPUPSTART", None, None, None, Some(7))
      when(qLogTransformer(qLogData)).thenReturn(EnterQueue(7))

      ref ! qLogData

      verify(qLogTransformer)(qLogData)
    }

    "send event to queue event dispatcher" in new Helper {
      val (ref, _) = actor()
      val qLogData = QueueLogData("2014-04-18 12:22:20.225112", "NONE", "ENTERQUEUE", None, None, None, Some(5))
      when(qLogTransformer(qLogData)).thenReturn(EnterQueue(5))

      ref ! qLogData

      eventObjQueueDispatcher.expectMsg(EnterQueue(5))
    }

  }
}