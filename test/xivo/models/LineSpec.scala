package xivo.models

import org.scalatestplus.play.OneAppPerSuite
import play.api.test.FakeApplication
import xuctest.{IntegrationTest, BaseTest}


class LineSpec extends BaseTest with OneAppPerSuite {

  implicit override lazy val app: FakeApplication =
    FakeApplication(
      additionalConfiguration = xivoIntegrationConfig
    )

  "xuc" should {
    "be able to get a list of lines from xivo" taggedAs(IntegrationTest) in {
        val lines = Line.all
        for(line <- lines) {
          println(line)
        }
        lines.length should be>(0)
    }
    "be able to get a line from xivo" taggedAs(IntegrationTest) in {
        val line = Line.get(37)
        println(line)
        line.get shouldBe a [Line]
    }
  }
}
