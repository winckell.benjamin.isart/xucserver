describe("Membership message function", function() {
    var testSocket ={};

    beforeEach(function() {
        testSocket = {
            sendCallback: function() {
                return true;
            },
            close: function() {
                return true;
            }
        };
        spyOn(testSocket, 'sendCallback');
        spyOn(testSocket, 'close');
        Cti.init("user", "agent", testSocket);
        Membership.init(Cti);
    });

    it("should send getUserDefaultMembership to websocket with userId", function() {
        spyOn(Membership.MessageFactory, 'createGetUserDefaultMembership');

        var userId = 123;
        Membership.getUserDefaultMembership(userId);
        expect(Membership.MessageFactory.createGetUserDefaultMembership).toHaveBeenCalledWith(userId);
        expect(testSocket.sendCallback).toHaveBeenCalled();
    });

    it("should send setUserDefaultMembership to websocket with userId and membership", function() {
        spyOn(Membership.MessageFactory, 'createSetUserDefaultMembership');

        var userId = 123;
        var membership = [{"queueId": 5, "penalty": 1}, {"queueId": 2, "penalty": 3}];
        Membership.setUserDefaultMembership(userId, membership);
        expect(Membership.MessageFactory.createSetUserDefaultMembership).toHaveBeenCalledWith(userId, membership);
        expect(testSocket.sendCallback).toHaveBeenCalled();
    });

    it("should send setUsersDefaultMembership to websocket with list of userId and membership", function() {
        spyOn(Membership.MessageFactory, 'createSetUsersDefaultMembership');

        var userIds = [1, 2, 3];
        var membership = [{"queueId": 5, "penalty": 1}, {"queueId": 2, "penalty": 3}];

        Membership.setUsersDefaultMembership(userIds, membership);
        expect(Membership.MessageFactory.createSetUsersDefaultMembership).toHaveBeenCalledWith(userIds, membership);
        expect(testSocket.sendCallback).toHaveBeenCalled();
    });

    it("should send applyUsersDefaultMembership to websocket with a list of userId", function() {
        spyOn(Membership.MessageFactory, 'createApplyUsersDefaultMembership');

        var userIds = [1, 2, 3];

        Membership.applyUsersDefaultMembership(userIds);
        expect(Membership.MessageFactory.createApplyUsersDefaultMembership).toHaveBeenCalledWith(userIds);
        expect(testSocket.sendCallback).toHaveBeenCalled();
    });
});