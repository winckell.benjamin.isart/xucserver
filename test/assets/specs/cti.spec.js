describe("CTI receive function", function() {
    it("should call subscriber on msgType", function () {

        var event = {};
        var message = {};
        message.msgType = Cti.MessageType.AGENTERROR;
        message.ctiMessage = "{ctiMessage}";
        event.data = JSON.stringify(message);

        var Callback = {
                handler: function() {
                  return true;
                }
            };

        spyOn(Callback, 'handler');
        Cti.setHandler(Cti.MessageType.AGENTERROR,Callback.handler);
        Cti.receive(event);
        expect(Callback.handler).toHaveBeenCalledWith(message.ctiMessage);
    });
});

describe("CTI functions", function() {
    var testSocket ={};

    beforeEach(function() {
        testSocket = {
                sendCallback: function() {
                  return true;
                },
                close: function() {
                  return true;
                }
        };
        spyOn(testSocket, 'sendCallback');
        spyOn(testSocket, 'close');
        Cti.init("user", "agent", testSocket);
    });

    it("should send agent login message to websocket", function() {
        var agentPhoneNumber = 2002;
        var message = Cti.WebsocketMessageFactory.createAgentLogin(agentPhoneNumber);

        Cti.loginAgent(agentPhoneNumber);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send agent login message to websocket with agentid", function() {
        spyOn(Cti.WebsocketMessageFactory, 'createAgentLogin');
        var agentPhoneNumber = 2002;
        var agentId = 32;
        Cti.loginAgent(agentPhoneNumber,agentId);
        expect(Cti.WebsocketMessageFactory.createAgentLogin).toHaveBeenCalledWith(agentPhoneNumber,agentId);
        expect(testSocket.sendCallback).toHaveBeenCalled();
    });

    it("should send agent logout message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createAgentLogout();

        Cti.logoutAgent();
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });
    it("should send agent logout message to websocket with agentId", function() {
        spyOn(Cti.WebsocketMessageFactory, 'createAgentLogout');
        var agentId = 77;
        var message = "{}";
        Cti.logoutAgent(agentId);
        expect(Cti.WebsocketMessageFactory.createAgentLogout).toHaveBeenCalledWith(agentId);
        expect(testSocket.sendCallback).toHaveBeenCalled();
    });

    it("should send pause agent message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createPauseAgent();

        Cti.pauseAgent();
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send pause agent with agentid message to websocket", function() {
        spyOn(Cti.WebsocketMessageFactory, 'createPauseAgent');
        var agentId = 41;

        Cti.pauseAgent(agentId);
        expect(Cti.WebsocketMessageFactory.createPauseAgent).toHaveBeenCalledWith(agentId);
        expect(testSocket.sendCallback).toHaveBeenCalled();
    });

    it("should send unpause agent message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createUnpauseAgent();

        Cti.unpauseAgent();
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send unpause agent with agent id message to websocket", function() {
        spyOn(Cti.WebsocketMessageFactory, 'createUnpauseAgent');
        var agentId = 88;

        var message = Cti.WebsocketMessageFactory.createUnpauseAgent();

        Cti.unpauseAgent(agentId);

        expect(Cti.WebsocketMessageFactory.createUnpauseAgent).toHaveBeenCalledWith(agentId);
        expect(testSocket.sendCallback).toHaveBeenCalled();
    });

    it('should send listen to an agent with agent id to websocket', function(){
          var msg = {"listenToAgent":1};
           var agentId = 456;

           var args = {};
           args.agentid = agentId;

           spyOn(Cti.WebsocketMessageFactory, 'createMessageFromArgs').and.returnValue(msg);

           Cti.listenAgent(agentId);

           expect(Cti.WebsocketMessageFactory.createMessageFromArgs).toHaveBeenCalledWith('listenAgent',args);
           expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);
    });
    it("should send user status message to websocket", function() {
        var status = "unavailable";
        var message = Cti.WebsocketMessageFactory.createUserStatusUpdate(status);

        Cti.changeUserStatus(status);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send dnd status change request to websocket", function() {
        var state = true;
        var message = Cti.WebsocketMessageFactory.createDnd(state);

        Cti.dnd(state);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send dial message to websocket", function() {
        var number = "06324567";
        var message = Cti.WebsocketMessageFactory.createDial(number);

        Cti.dial(number);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("Hangup should send hangup message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createHangup();

        Cti.hangup();
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("Answer should send answer message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createAnswer();

        Cti.answer();
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });
    it("Hold should send hold message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createHold();

        Cti.hold();
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });
    it("should send direct transfer message to websocket", function() {
        var destination = "23490";
        var message = Cti.WebsocketMessageFactory.createDirectTransfer(destination);

        Cti.directTransfer(destination);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send attended transfer message to websocket", function() {
        var destination = "78450";
        var message = Cti.WebsocketMessageFactory.createAttendedTransfer(destination);

        Cti.attendedTransfer(destination);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send complete transfer message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createCompleteTransfer();

        Cti.completeTransfer();
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send cancel transfer message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createCancelTransfer();

        Cti.cancelTransfer();
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("conference should send conference message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createConference();

        Cti.conference();
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send search directory message to websocket", function() {
        var pattern = "zuc";
        var message = Cti.WebsocketMessageFactory.createSearchDirectory(pattern);

        Cti.searchDirectory(pattern);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send directory look up message to websocket", function() {
        var pattern = "zuc";
        var message = Cti.WebsocketMessageFactory.createDirectoryLookUp(pattern);

        Cti.directoryLookUp(pattern);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send get favorites message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createGetFavorites();

        Cti.getFavorites();
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send add favorite message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createAddFavorite("123", "xivou");

        Cti.addFavorite("123", "xivou");
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send remove favorites message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createRemoveFavorite("234", "other");

        Cti.removeFavorite("234", "other");
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send SubscribeToQueueStats message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createSubscribeToQueueStats();

        Cti.subscribeToQueueStats();
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });
    it("should send SubscribeToAgentStats message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createSubscribeToAgentStats();

        Cti.subscribeToAgentStats();
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send getAgentStates message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createGetAgentStates();

        Cti.getAgentStates();
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });


    it("should send getQueueStatistics message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createGetQueueStatistics("1",1800,30);

        Cti.getQueueStatistics("1",1800,30);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });
    it("should send subscribeToAgentEvents message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createSubscribeToAgentEvents();

        Cti.subscribeToAgentEvents();
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });
    it("should send get config message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createGetConfig("queue");

        Cti.getConfig("queue");
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });
    it("should send get agent directory message to websocket",function() {
       var message = Cti.WebsocketMessageFactory.createGetAgentDirectory();
       Cti.getAgentDirectory();
       expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });
    it("should send get queue list to web socket", function() {
       var msg = {"GetList" :"queue"};
       spyOn(Cti.WebsocketMessageFactory, 'createGetList').and.callFake(function(objectType) {return msg;});

       Cti.getList("queue");

       expect(Cti.WebsocketMessageFactory.createGetList).toHaveBeenCalledWith('queue');
       expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);

    });
    it("should send set agent queue message to web socket", function() {
       var msg = {"SetAgentQueue":1};
       var agentId = "37";
       var queueId = "56";
       var penalty = 3;
       spyOn(Cti.WebsocketMessageFactory, 'createSetAgentQueue').and.callFake(function(agentId, queueId, penalty) {return msg;});

       Cti.setAgentQueue(agentId, queueId, penalty);

       expect(Cti.WebsocketMessageFactory.createSetAgentQueue).toHaveBeenCalledWith(agentId, queueId, penalty);
       expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);

    });
    it("should send remove agent from queue message to web socket", function() {
       var msg = {"RemoveAgentFromQueue":1};
       var agentId = "12";
       var queueId = "43";
       spyOn(Cti.WebsocketMessageFactory, 'createRemoveAgentFromQueue').and.callFake(function(agentId,queueId) {return msg;});

       Cti.removeAgentFromQueue(agentId,queueId);

       expect(Cti.WebsocketMessageFactory.createRemoveAgentFromQueue).toHaveBeenCalledWith(agentId,queueId);
       expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);

    });
    it('should send move group of agents to web socket', function() {
       var msg = {"moveAgentsInGroup":1};
       var groupId = 72;
       var fromQueueId = 830, fromPenalty = 7;
       var toQueueId = 120, toPenalty = 2;

       var args = {};
       args.groupId = groupId;
       args.fromQueueId = fromQueueId;
       args.fromPenalty = fromPenalty;
       args.toQueueId = toQueueId;
       args.toPenalty = toPenalty;

       spyOn(Cti.WebsocketMessageFactory, 'createMessageFromArgs').and.returnValue(msg);

       Cti.moveAgentsInGroup(groupId, fromQueueId, fromPenalty, toQueueId, toPenalty);

       expect(Cti.WebsocketMessageFactory.createMessageFromArgs).toHaveBeenCalledWith('moveAgentsInGroup',args);
       expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);

    });
    it('should send add group of agents to web socket', function() {
       var msg = {"addAgentsInGroup":1};
       var groupId = 72;
       var fromQueueId = 830, fromPenalty = 7;
       var toQueueId = 120, toPenalty = 2;

       var args = {};
       args.groupId = groupId;
       args.fromQueueId = fromQueueId;
       args.fromPenalty = fromPenalty;
       args.toQueueId = toQueueId;
       args.toPenalty = toPenalty;

       spyOn(Cti.WebsocketMessageFactory, 'createMessageFromArgs').and.returnValue(msg);

       Cti.addAgentsInGroup(groupId, fromQueueId, fromPenalty, toQueueId, toPenalty);

       expect(Cti.WebsocketMessageFactory.createMessageFromArgs).toHaveBeenCalledWith('addAgentsInGroup',args);
       expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);

    });
    it('should send remove agents from queue group to web socket', function() {
       var msg = {"removeAgentGroupFromQueueGroup":1},
       groupId = 72,
       queueId = 830, penalty = 7;

       var args = {'groupId':groupId, 'queueId':queueId, 'penalty':penalty};

       spyOn(Cti.WebsocketMessageFactory, 'createMessageFromArgs').and.returnValue(msg);

       Cti.removeAgentGroupFromQueueGroup(groupId, queueId, penalty);

       expect(Cti.WebsocketMessageFactory.createMessageFromArgs).toHaveBeenCalledWith('removeAgentGroupFromQueueGroup',args);
       expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);

    });
    it('should send add agent not in queue from group to queue penalty to web socket', function() {
       var msg = {"addAgentsNotInQueueFromGroupTo":1},
       groupId = 64,
       queueId = 238, penalty = 4;

       var args = {'groupId':groupId, 'queueId':queueId, 'penalty':penalty};

       spyOn(Cti.WebsocketMessageFactory, 'createMessageFromArgs').and.returnValue(msg);

       Cti.addAgentsNotInQueueFromGroupTo(groupId, queueId, penalty);

       expect(Cti.WebsocketMessageFactory.createMessageFromArgs).toHaveBeenCalledWith('addAgentsNotInQueueFromGroupTo',args);
       expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);

    });
    it("should send pause monitor with agent id message to websocket", function() {
        var msg = {"monitorPause":1}
        var agentId = 332;

        var args = {};
        args.agentid = agentId;

        spyOn(Cti.WebsocketMessageFactory, 'createMessageFromArgs').and.returnValue(msg);

        Cti.monitorPause(agentId);

        expect(Cti.WebsocketMessageFactory.createMessageFromArgs).toHaveBeenCalledWith('monitorPause',args);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);
    });
    it("should send unpause monitor with agent id message to websocket", function() {
        var msg = {"monitorUnpause":1}
        var agentId = 332;

        var args = {};
        args.agentid = agentId;

        spyOn(Cti.WebsocketMessageFactory, 'createMessageFromArgs').and.returnValue(msg);

        Cti.monitorUnpause(agentId);

        expect(Cti.WebsocketMessageFactory.createMessageFromArgs).toHaveBeenCalledWith('monitorUnpause',args);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);
    });

    it("should send na forward to websocket", function(){
        var msg = {"naFwd":1}
        var destination = "3456";
        var state = true;

        var args = {};
        args.destination = destination;
        args.state = state;

        spyOn(Cti.WebsocketMessageFactory, 'createMessageFromArgs').and.returnValue(msg);

        Cti.naFwd(destination,state);

        expect(Cti.WebsocketMessageFactory.createMessageFromArgs).toHaveBeenCalledWith('naFwd',args);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);

    });

    it("should send unc forward to websocket", function(){
        var msg = {"uncFwd":1}
        var destination = "7789";
        var state = true;

        var args = {};
        args.destination = destination;
        args.state = state;

        spyOn(Cti.WebsocketMessageFactory, 'createMessageFromArgs').and.returnValue(msg);

        Cti.uncFwd(destination,state);

        expect(Cti.WebsocketMessageFactory.createMessageFromArgs).toHaveBeenCalledWith('uncFwd',args);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);

    });
    it("should send busy forward to websocket", function(){
        var msg = {"busyFwd":1}
        var destination = "8899";
        var state = true;

        var args = {};
        args.destination = destination;
        args.state = state;

        spyOn(Cti.WebsocketMessageFactory, 'createMessageFromArgs').and.returnValue(msg);

        Cti.busyFwd(destination,state);

        expect(Cti.WebsocketMessageFactory.createMessageFromArgs).toHaveBeenCalledWith('busyFwd',args);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(msg);

    });

    it("should close the websocket", function() {
        Cti.close();

        expect(testSocket.close).toHaveBeenCalled();
    });

    it("should send getConferenceRooms message to websocket", function() {
        var message = Cti.WebsocketMessageFactory.createGetList("meetme");

        Cti.getConferenceRooms();
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });

    it("should send inviteConferenceRoom message to websocket", function() {
        var userId = 5;
        var message = Cti.WebsocketMessageFactory.createInviteConferenceRoom(userId);

        Cti.inviteConferenceRoom(userId);
        expect(testSocket.sendCallback).toHaveBeenCalledWith(message);
    });
});
    var expectWebMessage = function(message, cmd) {
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(cmd);
    };

describe("WebsocketMessageFactory agent queue", function() {
    it("should create set agent queue message", function() {
       var agentId = 12;
       var queueId = 23;
       var penalty = 9;

       var message = Cti.WebsocketMessageFactory.createSetAgentQueue(agentId, queueId, penalty);
       expectWebMessage(message,Cti.WebsocketMessageFactory.setAgentQueueCmd);
       expect(message.agentId).toBe(agentId);
       expect(message.queueId).toBe(queueId);
       expect(message.penalty).toBe(penalty);

    });
    it("should create remove agent from queue message", function() {
       var agentId = 10;
       var queueId = 20;

       var message = Cti.WebsocketMessageFactory.createRemoveAgentFromQueue(agentId,queueId);
       expectWebMessage(message,Cti.WebsocketMessageFactory.removeAgentFromQueueCmd);
       expect(message.agentId).toBe(agentId);
       expect(message.queueId).toBe(queueId);

    });
});

describe("WebsocketMessageFactory createAgentLogin function", function() {
    it("should create agent login message", function() {
        var agentPhoneNumber = 2002;
        var message = Cti.WebsocketMessageFactory.createAgentLogin(agentPhoneNumber);
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.agentLoginCmd);
        expect(message.agentphonenumber).toBe(agentPhoneNumber);

    });
    it("should create agent login message with agentid", function() {
        var agentPhoneNumber = 2002;
        var agentId = 32;
        var message = Cti.WebsocketMessageFactory.createAgentLogin(agentPhoneNumber,agentId);
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.agentLoginCmd);
        expect(message.agentphonenumber).toBe(agentPhoneNumber);
        expect(message.agentid).toBe(agentId);

    });
});

describe("WebsocketMessageFactory createAgentLogout function", function() {

    it("should create agent logout message", function() {
        var message = Cti.WebsocketMessageFactory.createAgentLogout();
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.agentLogoutCmd);
    });
    it("should create agent logout message with agentId", function() {
        var agentId = 55;
        var message = Cti.WebsocketMessageFactory.createAgentLogout(agentId);
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.agentLogoutCmd);
        expect(message.agentid).toBe(agentId);
    });
});
describe("WebsocketMessageFactory createPauseAgent function", function() {
    it("should create pause agent message", function() {
        var message = Cti.WebsocketMessageFactory.createPauseAgent();
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.pauseAgentCmd);
    });
    it("should create pause agent message with agentid", function() {
        var agentId = 75;
        var message = Cti.WebsocketMessageFactory.createPauseAgent(agentId);
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.pauseAgentCmd);
        expect(message.agentid).toBe(agentId);
    });
});

describe("WebsocketMessageFactory createUnpauseAgent function", function() {
    it("should create unpause agent message", function() {
        var message = Cti.WebsocketMessageFactory.createUnpauseAgent();
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.unpauseAgentCmd);
    });
    it("should create unpause agent message with agent id", function() {
        var agentId = 12;
        var message = Cti.WebsocketMessageFactory.createUnpauseAgent(agentId);
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.unpauseAgentCmd);
        expect(message.agentid).toBe(agentId);
    });
});
describe("WebsocketMessageFactory createUserStatusUpdate function", function() {
    it("should create UserStatusUpdate message", function() {
        var message = Cti.WebsocketMessageFactory.createUserStatusUpdate("available");
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.userStatusUpdateCmd);
        expect(message.status).toBe("available");
    });
});


describe("WebsocketMessageFactory createDnd function", function() {
    it("should create dnd message", function() {
        var message = Cti.WebsocketMessageFactory.createDnd(true);
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.dndCmd);
        expect(message.state).toBe(true);
    });
});

describe("WebsocketMessageFactory createDial function", function() {
    it("should create dial message", function() {
        var message = Cti.WebsocketMessageFactory.createDial("1234");
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.dialCmd);
        expect(message.destination).toBe("1234");
    });
});

describe("WebsocketMessageFactory createHangup function", function() {
    it("should create hangup message", function() {

        var message = Cti.WebsocketMessageFactory.createHangup();

        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.hangupCmd);
    });
});

describe("WebsocketMessageFactory createHold function", function() {
    it("should create hold message", function() {

        var message = Cti.WebsocketMessageFactory.createHold();

        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.holdCmd);
    });
});

describe("WebsocketMessageFactory createAnswer function", function() {
    it("should create answer message", function() {

        var message = Cti.WebsocketMessageFactory.createAnswer();

        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.answerCmd);
    });
});

describe("WebsocketMessageFactory createDirectTransfer function", function() {
    it("should create direct transfer message", function() {
        var message = Cti.WebsocketMessageFactory.createDirectTransfer("1234");
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.directTransferCmd);
        expect(message.destination).toBe("1234");
    });
});

describe("WebsocketMessageFactory createAttendedTransfer function", function() {
    it("should create attended transfer message", function() {
        var message = Cti.WebsocketMessageFactory.createAttendedTransfer("7654");
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.attendedTransferCmd);
        expect(message.destination).toBe("7654");
    });
});
describe("WebsocketMessageFactory createCompleteTransfer function", function() {
    it("should create complete transfer message", function() {

        var message = Cti.WebsocketMessageFactory.createCompleteTransfer();

        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.completeTransferCmd);
    });
});
describe("WebsocketMessageFactory createCancelTransfer function", function() {
    it("should create cancel transfer message", function() {

        var message = Cti.WebsocketMessageFactory.createCancelTransfer();

        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.cancelTransferCmd);
    });
});
describe("WebsocketMessageFactory createConference function", function() {
    it("should create conference message", function() {

        var message = Cti.WebsocketMessageFactory.createConference();

        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.conferenceCmd);
    });
});
describe("WebsocketMessageFactory createSearchDirectory function", function() {
    it("should create search directory message", function() {
        var message = Cti.WebsocketMessageFactory.createSearchDirectory("abc");
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.searchDirectoryCmd);
        expect(message.pattern).toBe("abc");
    });
});

describe("WebsocketMessageFactory createDirectoryLookUp function", function() {
    it("should create directory look up message", function() {
        var message = Cti.WebsocketMessageFactory.createDirectoryLookUp("abc");
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.directoryLookUpCmd);
        expect(message.term).toBe("abc");
    });
});

describe("WebsocketMessageFactory getFavorites function", function() {
    it("should create get favorites message", function() {
        var message = Cti.WebsocketMessageFactory.createGetFavorites();
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.getFavoritesCmd);
    });
});

describe("WebsocketMessageFactory addFavorite function", function() {
    it("should create set favorite message", function() {
        var message = Cti.WebsocketMessageFactory.createAddFavorite("123", "xivou");
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.addFavoriteCmd);
    });
});

describe("WebsocketMessageFactory removeFavorites function", function() {
    it("should create remove favorites message", function() {
        var message = Cti.WebsocketMessageFactory.createRemoveFavorite("234", "other");
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.removeFavoriteCmd);
    });
});

describe("WebsocketMessageFactory SubscribeToQueueStats function", function() {
    it("should create SubscribeToQueueStats message", function() {
        var message = Cti.WebsocketMessageFactory.createSubscribeToQueueStats();
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.subscribeToQueueStatsCmd);
    });
});

describe("WebsocketMessageFactory SubscribeToAgentStats function", function() {
    it("should create SubscribeToAgentStats message", function() {
        var message = Cti.WebsocketMessageFactory.createSubscribeToAgentStats();
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.subscribeToAgentStatsCmd);
    });
});

describe("WebsocketMessageFactory GetAgentStates function", function() {
    it("should create getAgentStates message", function() {
        var message = Cti.WebsocketMessageFactory.createGetAgentStates();
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.getAgentStatesCmd);
    });
});

describe("WebsocketMessageFactory createGetQueueStatistics function", function() {
    it("should create createGetQueueStatistics message", function() {
        var message = Cti.WebsocketMessageFactory.createGetQueueStatistics("34",1800,30);
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.getQueueStatisticsCmd);
        expect(message.queueId).toBe("34");
        expect(message.window).toBe(1800);
        expect(message.xqos).toBe(30);
    });
});

describe("WebsocketMessageFactory SubscribeToAgentEvents function", function() {
    it("should create SubscribeToAgentEvents message", function() {
        var message = Cti.WebsocketMessageFactory.createSubscribeToAgentEvents();
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.subscribeToAgentEventsCmd);
    });
});

describe("WebsocketMessageFactory createGetConfig function", function() {
    it("should create getConfig message", function() {
        var message = Cti.WebsocketMessageFactory.createGetConfig("queue");
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.getConfigCmd);
        expect(message.objectType).toBe("queue");
    });
});
describe("WebsocketMessageFactory createGetList function", function() {
    it("should create getList message", function() {
        var message = Cti.WebsocketMessageFactory.createGetList("queue");
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.command).toBe(Cti.WebsocketMessageFactory.getListCmd);
        expect(message.objectType).toBe("queue");
    });
});

describe("WebsocketMessageFactory createGetAgentDirectory function", function() {
        it("should create GetAgentDirectory message", function() {
            var message = Cti.WebsocketMessageFactory.createGetAgentDirectory();
            expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
            expect(message.command).toBe(Cti.WebsocketMessageFactory.getAgentDirectoryCmd);
        });
});
describe("WebsocketMessageFactory createPing function", function() {
    it("should create ping message", function() {
        var message = Cti.WebsocketMessageFactory.createPing();
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.pingClaz);
    });
});
describe("WebsocketMessageFactory createFrom args", function() {
    it("should create web message with arguments", function() {
        var args ={'one':32, 'two':'hello'};

        var message = Cti.WebsocketMessageFactory.createMessageFromArgs('messageCommand', args);
        expect(message.claz).toBe(Cti.WebsocketMessageFactory.messageClaz);
        expect(message.one).toBe(32);
        expect(message.two).toBe('hello');
    });
});
