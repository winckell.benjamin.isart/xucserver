package services.request

import services.XucAmiBus.{SetVarActionRequest, SetVarRequest}
import xuctest.BaseTest

class KeyLightRequestSpec extends BaseTest {

  "Turning light on" should {

    "be transformed to ami request for pause key" in {
      val phoneNb = "24300"
      val turnOnPause = TurnOnKeyLight(phoneNb, PhoneKey.Pause)

      turnOnPause.toAmi should be(SetVarRequest(SetVarActionRequest(s"DEVICE_STATE(Custom:***34$phoneNb)","INUSE")))
    }
    "be transformed to ami request for logon key" in {
      val phoneNb = "27300"
      val turnOnPause = TurnOnKeyLight(phoneNb, PhoneKey.Logon)

      turnOnPause.toAmi should be(SetVarRequest(SetVarActionRequest(s"DEVICE_STATE(Custom:***30$phoneNb)","INUSE")))
    }
  }
  "Turning light off" should {

    "be transformed to ami request for pause key" in {
      val phoneNb = "77300"
      val turnOffPause = TurnOffKeyLight(phoneNb, PhoneKey.Pause)

      turnOffPause.toAmi should be(SetVarRequest(SetVarActionRequest(s"DEVICE_STATE(Custom:***34$phoneNb)","NOT_INUSE")))
    }
    "be transformed to ami request for logon key" in {
      val phoneNb = "27300"
      val turnOffLogon = TurnOffKeyLight(phoneNb, PhoneKey.Logon)

      turnOffLogon.toAmi should be(SetVarRequest(SetVarActionRequest(s"DEVICE_STATE(Custom:***30$phoneNb)","NOT_INUSE")))
    }
  }

}
