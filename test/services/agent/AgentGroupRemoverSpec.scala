package services.agent

import akka.actor.Props
import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import services.ActorFactory
import services.agent.AgentInGroupAction.{RemoveAgents, AgentsDestination}
import services.config.ConfigDispatcher._
import xivo.models.Agent


class AgentGroupRemoverSpec extends TestKitSpec("agentingroupremover") {


   class Helper {
     val configDispatcher = TestProbe()

     trait TestActorFactory extends  ActorFactory {
       override val configDispatcherURI = configDispatcher.ref.path.toString
     }

     def actor(groupId: Long, queueId: Long, penalty : Int) = {
       val a = TestActorRef(Props(new AgentGroupRemover(groupId, queueId, penalty) with TestActorFactory))
       (a,a.underlyingActor)
     }
   }



   "an agent group remover" should {

     "should remove agent group from queue penalty on RemoveAgents command" in new Helper {

       val (groupId, queueId, penalty) = (9,52,7)
       val (ref,_) = actor(groupId,queueId,penalty)

       val agents = List(Agent(1,"Kim","Notch","4589","browser",groupId))

       ref ! RemoveAgents

       ref !  AgentList(agents)

       configDispatcher.expectMsgAllOf(RequestConfig(ref,GetAgents(groupId, queueId, penalty)),
                                       ConfigChangeRequest(ref,RemoveAgentFromQueue(1,queueId))
                                       )

     }
   }
 }
