package services.agent

import akka.actor.{ActorRef, Props}
import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import models.{QueueMembership, UserIdList, UserQueueDefaultMembership, UsersQueueMembership}
import org.mockito.Mockito.{stub, verify}
import org.scalatest.mock.MockitoSugar
import services.ActorFactory
import services.agent.AgentInGroupAction.{AgentsDestination, RemoveAgents}
import services.config.ConfigDispatcher.{ConfigChangeRequest, GetAgentByUserId, GetQueuesForAgent, QueuesForAgent, RemoveAgentFromQueue, RequestConfig}
import services.config.ConfigServerRequester
import services.request._
import xivo.models.{Agent, AgentQueueMember}

import scala.concurrent.Future

class AgentConfigSpec extends TestKitSpec("AgentConfig") with MockitoSugar {

  class Helper {

    val requester: ConfigServerRequester = mock[ConfigServerRequester]
    val moveAgentInGroups = TestProbe()
    val addAgentInGroups = TestProbe()
    val removeAgentGroupFromQueueGroup = TestProbe()
    val addAgentsNotInQueueFromGroupTo = TestProbe()
    val agentGroupSetter = TestProbe()
    val configDispatcher = TestProbe()

    val mockMove = mock[(Long,Long,Int)=> ActorRef]
    val mockEmpty = mock[() => ActorRef]

    trait TestActorFactory extends ActorFactory {
      override def getMoveAgentInGroup(groupId : Long, queueId: Long, penalty : Int) = mockMove(groupId, queueId, penalty)
      override def getAddAgentInGroup(groupId : Long, queueId: Long, penalty : Int) = mockMove(groupId, queueId, penalty)
      override def getRemoveAgentGroupFromQueueGroup(groupId: Long, queueId: Long, penalty: Int) = mockMove(groupId, queueId, penalty)
      override def getAddAgentsNotInQueueFromGroupTo(groupId: Long, queueId: Long, penalty: Int) = mockMove(groupId, queueId, penalty)
      override def getAgentGroupSetter() = mockEmpty()
      override val configDispatcherURI = configDispatcher.ref.path.toString
    }

    def actor = {
      val a = TestActorRef[AgentConfig](Props(new AgentConfig(requester) with TestActorFactory))
      (a, a.underlyingActor)
    }
  }

  "Agent config" should {
    "delegate move agent from a group in queue/penaly to a queue/penalty to an actor" in new Helper {
      val (ref, agentConfig) = actor

      val moveRequest = MoveAgentInGroup(groupId = 3,fromQueueId = 45, fromPenalty = 2, toQueueId = 89, toPenalty = 1)

      stub(agentConfig.getMoveAgentInGroup(3,45,2)).toReturn(moveAgentInGroups.ref)

      ref ! BaseRequest(TestProbe().ref,moveRequest)

      moveAgentInGroups.expectMsg(AgentsDestination(89,1))

    }
    "delegate add agent from a group in queue/penalty to a queue/penalty to an actor" in new Helper {
      val (groupId, fromQueueId, fromPenalty) = (7,44,8)
      val (toQueueId, toPenalty) = (22,2)

      val (ref, agentConfig) = actor

      val addRequest = AddAgentInGroup(groupId,fromQueueId,fromPenalty, toQueueId, toPenalty)

      stub(agentConfig.getAddAgentInGroup(groupId,fromQueueId,fromPenalty)).toReturn(addAgentInGroups.ref)

      ref ! BaseRequest(TestProbe().ref,addRequest)

      addAgentInGroups.expectMsg(AgentsDestination(toQueueId,toPenalty))
    }
    "delegate remove agent group from a queue penalty to an actor" in new Helper {
      val (groupId, queueId, penalty) = (7,44,8)

      val (ref, agentConfig) = actor

      val removeRequest = RemoveAgentGroupFromQueueGroup(groupId, queueId, penalty)

      stub(agentConfig.getRemoveAgentGroupFromQueueGroup(groupId,queueId,penalty)).toReturn(removeAgentGroupFromQueueGroup.ref)

      ref ! BaseRequest(TestProbe().ref,removeRequest)

      removeAgentGroupFromQueueGroup.expectMsg(RemoveAgents)

    }
    "delegate add agents from a group to a queue penalty to an actor" in new Helper {
      val (groupId, toQueueId, toPenalty) = (904,211,2)

      val (ref, agentConfig) = actor

      val addRequest = AddAgentsNotInQueueFromGroupTo(groupId, toQueueId, toPenalty)

      stub(agentConfig.getAddAgentsNotInQueueFromGroupTo(groupId,toQueueId,toPenalty)).toReturn(addAgentsNotInQueueFromGroupTo.ref)

      ref ! BaseRequest(TestProbe().ref,addRequest)

      addAgentsNotInQueueFromGroupTo.expectMsg(AgentsDestination(toQueueId,toPenalty))
    }

    "delegate set agent group to an actor" in new Helper {
      val (groupId, agentId) = (12, 25)
      val (ref, agentConfig) = actor
      stub(agentConfig.getAgentGroupSetter()).toReturn(agentGroupSetter.ref)

      ref ! BaseRequest(TestProbe().ref, SetAgentGroup(groupId, agentId))

      agentGroupSetter.expectMsg(SetAgentGroup(groupId, agentId))
    }

    "get agent default membership" in new Helper {
      val (ref, agentConfig) = actor
      val userId = 123
      val membership = List(QueueMembership(1,5), QueueMembership(8,1))
      val reqActor = TestProbe()
      stub(requester.getUserDefaultMembership(userId)).toReturn(Future.successful(membership))

      ref ! BaseRequest(reqActor.ref, GetUserDefaultMembership(userId))

      reqActor.expectMsg(UserQueueDefaultMembership(userId, membership))

      verify(requester).getUserDefaultMembership(userId)

    }

    "set agent default membership" in new Helper {
      val (ref, agentConfig) = actor
      val userId = 123
      val membership = List(QueueMembership(1,5), QueueMembership(8,1))
      stub(requester.setUserDefaultMembership(userId, membership)).toReturn(Future.successful(()))

      ref ! BaseRequest(TestProbe().ref, SetUserDefaultMembership(userId, membership))

      verify(requester).setUserDefaultMembership(userId, membership)
    }

    "apply default membership to a set of users" in new Helper {
      val (ref, agentConfig) = actor

      val userId1 = 1l
      val agentId1 = userId1 + 10
      val userId2 = 2l
      val userId3 = 5l
      val agentId3 = userId3 + 10

      val userIds = List(userId1,userId2,userId3)

      // Active membership
      val user1Membership = List(AgentQueueMember(agentId1, 1, 2), AgentQueueMember(agentId1, 3, 5))
      val user3Membership = List(AgentQueueMember(agentId3, 3, 5))

      // Default membership
      val defaultMembership = List(
        UserQueueDefaultMembership(userId1, List(QueueMembership(1,2), QueueMembership(2,3))),
        UserQueueDefaultMembership(userId3, List(QueueMembership(1,4), QueueMembership(2,5)))
      )

      stub(requester.findUsersDefaultMembership(UserIdList(userIds))).toReturn(Future.successful(defaultMembership))

      ref ! BaseRequest(ref, ApplyUsersDefaultMembership(userIds))

      configDispatcher.expectMsg(RequestConfig(ref, GetAgentByUserId(userId1)))
      configDispatcher.reply(Agent(agentId1, "James", "Bond", "3000", "", 0, userId1))

      configDispatcher.expectMsg(RequestConfig(ref, GetAgentByUserId(userId3)))
      configDispatcher.reply(Agent(agentId3, "Jason", "Bourne", "3000", "", 0, userId3))

      configDispatcher.expectMsg(RequestConfig(ref, GetQueuesForAgent(agentId1)))
      configDispatcher.reply(QueuesForAgent(user1Membership, agentId1))

      configDispatcher.expectMsg(RequestConfig(ref, GetQueuesForAgent(agentId3)))
      configDispatcher.reply(QueuesForAgent(user3Membership, agentId3))

      configDispatcher.expectMsg(ConfigChangeRequest(ref, RemoveAgentFromQueue(agentId1, 3)))
      configDispatcher.expectMsg(ConfigChangeRequest(ref, SetAgentQueue(agentId1, 2, 3)))

      configDispatcher.expectMsg(ConfigChangeRequest(ref, RemoveAgentFromQueue(agentId3, 3)))
      configDispatcher.expectMsg(ConfigChangeRequest(ref, SetAgentQueue(agentId3, 1, 4)))
      configDispatcher.expectMsg(ConfigChangeRequest(ref, SetAgentQueue(agentId3, 2, 5)))

    }

    "set default membership for a list of users" in new Helper {
      val (ref, agentConfig) = actor
      val userIds = List(1l,2l,3l)
      val uqm = UsersQueueMembership(userIds, List(QueueMembership(1,5), QueueMembership(8,1)))
      stub(requester.setUsersDefaultMembership(uqm)).toReturn(Future.successful(()))

      ref ! BaseRequest(TestProbe().ref, SetUsersDefaultMembership(userIds, uqm.membership))

      verify(requester).setUsersDefaultMembership(uqm)
    }
  }

}
