package services.config

import java.util.UUID

import akka.pattern.ask
import akka.testkit.{TestActorRef, TestProbe}
import akka.util.Timeout
import akkatest.TestKitSpec
import models._
import org.joda.time.{LocalDate, DateTime}
import org.mockito.Matchers.any
import org.mockito.Mockito._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.mock.MockitoSugar
import org.xivo.cti.message.QueueConfigUpdate
import services.request.PhoneRequest.Dial
import services.request._
import services.{ActorFactory, XucEventBus}
import xivo.events.{PhoneEvent, PhoneEventType}
import xivo.models.Agent
import xivo.network.{RecordingWS, WebServiceException}
import xivo.websocket.WsBus.WsContent
import xivo.websocket.{WSMsgType, WebSocketEvent}
import xivo.xuc.api.{RequestError, RequestResult, RequestSuccess}

import scala.concurrent.Future
import scala.concurrent.duration.DurationInt

class CallbackManagerSpec extends TestKitSpec("ConfigServerSpec")  with MockitoSugar with ScalaFutures {


  class Helper {
    val requester: ConfigServerRequester = mock[ConfigServerRequester]
    val recordingws: RecordingWS = mock[RecordingWS]
    val bus = mock[XucEventBus]
    ConfigRepository.repo = mock[ConfigRepository]
    val router = TestProbe()

    trait TestActorFactory extends ActorFactory {
      override def getCtiRouterUri(username: String) = router.ref.path.toString()
    }

    def actor() = {
      val a = TestActorRef(new CallbackManager(requester, recordingws, bus) with TestActorFactory)
      (a, a.underlyingActor)
    }

  }

  "CallbackManager actor " should {
    "retrieve the callback lists" in new Helper {
      val listUuid = UUID.randomUUID()
      val cbUuid = UUID.randomUUID()
      val callbacks = List(CallbackList(
        Some(listUuid), "The List", 3, List(
          CallbackRequest(
            Some(cbUuid),
            listUuid,
            Some("1000"),
            None,
            None,
            None,
            Some("The company"),
            None))
      ))
      stub(requester.getCallbackLists).toReturn(Future.successful(callbacks))
      val (ref, a) = actor()

      ref ! BaseRequest(self, GetCallbackLists)

      expectMsg(CallbackLists(callbacks))
    }

    "return an error if the webservice call fails" in new Helper {
      stub(requester.getCallbackLists).toReturn(Future.failed(new WebServiceException("Error message")))
      val (ref, a) = actor()

      ref ! BaseRequest(self, GetCallbackLists)

      expectMsg(WsContent(WebSocketEvent.createError(WSMsgType.Error, "Error message")))
    }

    "send the CSV, return RequestSuccess and publish the callback list on the bus" in new Helper {
      val listUuid = UUID.randomUUID()
      val cbUuid = UUID.randomUUID()
      val csv = ""
      val callbacks = List(CallbackList(
        Some(listUuid), "The List", 3, List(
          CallbackRequest(
            Some(cbUuid),
            listUuid,
            Some("1000"),
            None,
            None,
            None,
            Some("The company"),
            None))
      ))
      stub(requester.importCsvCallback(listUuid.toString, csv)).toReturn(Future.successful(Unit))
      stub(requester.getCallbackLists).toReturn(Future.successful(callbacks))
      val (ref, a) = actor()

      ref ! ImportCsvCallback(listUuid.toString, csv)

      expectMsg(RequestSuccess("CSV successfully imported"))
      //This is because of getCallbackList returning a Future... no idea how to do it cleanly
      Thread.sleep(100)
      verify(bus).publish(callbacks)
    }

    "send the CSV and return RequestError in case of error" in new Helper {
      val uuid = UUID.randomUUID().toString
      val csv = ""
      stub(requester.importCsvCallback(uuid, csv)).toReturn(Future.failed(new WebServiceException("Error message")))
      val (ref, a) = actor()

      ref ! ImportCsvCallback(uuid.toString, csv)

      expectMsg(RequestError("Error message"))
    }

    "take a callback for an agent" in new Helper {
      val uuid = UUID.randomUUID()
      val userName = "doe"
      val agent = Agent(12, "John", "Doe", "1000", "default")
      stub(ConfigRepository.repo.agentFromUsername(userName)).toReturn(Some(agent))
      stub(requester.takeCallback(uuid.toString, agent)).toReturn(Future.successful(Unit))
      val (ref, a) = actor()

      ref ! TakeCallbackWithAgent(uuid.toString, userName)

      verify(requester).takeCallback(uuid.toString, agent)
      Thread.sleep(100)
      verify(bus).publish(CallbackTaken(uuid, agent.id))
    }

    "release a callback" in new Helper {
      val uuid = UUID.randomUUID()
      stub(requester.releaseCallback(uuid.toString)).toReturn(Future.successful(Unit))
      val (ref, a) = actor()

      ref ! ReleaseCallback(uuid.toString)

      verify(requester).releaseCallback(uuid.toString)
      Thread.sleep(100)
      verify(bus).publish(CallbackReleased(uuid))
    }

    """upon reception of StartCallbackWithUser:
      |- retrieve the callback request
      |- create a callback ticket
      |- dial the given number with the provided uuid
      |- return CallbackTaken""" in new Helper {
      val rqUuid = UUID.randomUUID()
      val listUuid = UUID.randomUUID()
      val ticketUuid = UUID.randomUUID()
      val number = "3235648"
      val username = "j.doe"
      val agent = Agent(12, "John", "Doe", "1000", "default")
      val queue = mock[QueueConfigUpdate]
      stub(queue.getName).toReturn("thequeue")
      val request = CallbackRequest(Some(rqUuid), listUuid, Some("1000"), None, None, None, None, None, Some(agent.id), Some(12))
      val ticket = CallbackTicket(Some(ticketUuid), listUuid, rqUuid, "thequeue", agent.number, request.dueDate, DateTime.now(), None)
      stub(ConfigRepository.repo.agentFromUsername(username)).toReturn(Some(agent))
      stub(requester.getCallbackRequest(rqUuid.toString)).toReturn(Future.successful(request))
      stub(ConfigRepository.repo.getQueue(12)).toReturn(Some(queue))
      stub(recordingws.createCallbackTicket(any(classOf[CallbackTicket]))).toReturn(Future.successful(ticket))
      val (ref, a) = actor()

      ref ! BaseRequest(self, StartCallbackWithUser(rqUuid.toString, "1000", "j.doe"))

      router.expectMsg(BaseRequest(self, Dial("1000", Map("CALLBACK_TICKET_UUID" -> ticketUuid.toString))))
      expectMsg(CallbackStarted(rqUuid, ticketUuid))
    }

    "upon reception of UpdateCallbackTicket, transform it to CallbackTicketPatch and ask for update" in new Helper {
      val uuid = UUID.randomUUID()
      val msg = UpdateCallbackTicket(uuid.toString, Some(CallbackStatus.Answered), Some("Small comment"))
      val (ref, a) = actor()

      ref ! BaseRequest(self, msg)

      verify(recordingws).updateCallbackTicket(uuid.toString, CallbackTicketPatch(Some(CallbackStatus.Answered), Some("Small comment"), None))
    }

    """upon reception of UpdateCallbackTicket, if the status is not Callback:
      - cloture the associated request
      - send a CallbackClotured notification""" in new Helper {
      val ticketUuid = UUID.randomUUID()
      val requestUuid = UUID.randomUUID()
      val ticket = CallbackTicket(Some(ticketUuid), UUID.randomUUID(), requestUuid, "theQueue", "1000", new LocalDate(), new DateTime, None)
      val msg = UpdateCallbackTicket(ticketUuid.toString, Some(CallbackStatus.Answered), Some("Small comment"))
      stub(recordingws.getCallbackTicket(ticketUuid.toString)).toReturn(Future.successful(ticket))
      stub(requester.clotureRequest(requestUuid)).toReturn(Future.successful(Unit))
      val (ref, a) = actor()

      ref ! BaseRequest(self, msg)

      Thread.sleep(100)
      verify(requester).clotureRequest(requestUuid)
      verify(bus).publish(CallbackClotured(requestUuid))
    }

    "upon reception of UpdateCallbackTicket, uncloture the associated request if the status is Callback" in new Helper {
      val ticketUuid = UUID.randomUUID()
      val requestUuid = UUID.randomUUID()
      val ticket = CallbackTicket(Some(ticketUuid), UUID.randomUUID(), requestUuid, "theQueue", "1000", new LocalDate(), new DateTime, None)
      val msg = UpdateCallbackTicket(ticketUuid.toString, Some(CallbackStatus.Callback), Some("Small comment"))
      stub(recordingws.getCallbackTicket(ticketUuid.toString)).toReturn(Future.successful(ticket))
      val (ref, a) = actor()

      ref ! BaseRequest(self, msg)

      Thread.sleep(100)
      verify(requester).unclotureRequest(requestUuid)
    }

    "upon reception of UpdateCallbackTicket, not uncloture nor cloture the associated request if the status is undefined" in new Helper {
      val ticketUuid = UUID.randomUUID()
      val requestUuid = UUID.randomUUID()
      val ticket = CallbackTicket(Some(ticketUuid), UUID.randomUUID(), requestUuid, "theQueue", "1000", new LocalDate(), new DateTime, None)
      val msg = UpdateCallbackTicket(ticketUuid.toString, None, Some("Small comment"))
      stub(recordingws.getCallbackTicket(ticketUuid.toString)).toReturn(Future.successful(ticket))
      val (ref, a) = actor()

      ref ! BaseRequest(self, msg)

      Thread.sleep(100)
      verifyNoMoreInteractions(requester)
    }

    "ask for the CSV of callback tickets" in new Helper {
      val uuid = UUID.randomUUID().toString
      val csv = "the csv content"
      implicit val timeout = Timeout(100 milliseconds)
      stub(recordingws.exportTicketsCSV(uuid)).toReturn(Future.successful(csv))
      val (ref, a) = actor()

      val res = (ref ? ExportTicketsCsv(uuid)).mapTo[RequestResult]

      res.futureValue shouldEqual RequestSuccess(csv)
    }

    "ask for the CSV of callback tickets and return RequestError in case of failure" in new Helper {
      val uuid = UUID.randomUUID().toString
      implicit val timeout = Timeout(100 milliseconds)
      stub(recordingws.exportTicketsCSV(uuid)).toReturn(Future.failed(new WebServiceException("Error message")))
      val (ref, a) = actor()

      val res = (ref ? ExportTicketsCsv(uuid)).mapTo[RequestResult]

      res.futureValue shouldEqual RequestError("Error message")
    }

    "subscribe to PhoneEvents when starting" in new Helper {
      val (ref, a) = actor()
      verify(bus).subscribe(ref, XucEventBus.allPhoneEventsTopic)
    }

    "update the proper callback ticket with a callid when receiving an EventDialing with a variable USR_CALLBACK_TICKET_UUID" in new Helper {
      val callid = "123456.777"
      val ticketUuid = UUID.randomUUID()
      val event = PhoneEvent(PhoneEventType.EventDialing, "Test", "Test", uniqueId="123456.888", linkedId = callid,
        userData = Map("XIVO_SRCNUM" -> "1000", "USR_CALLBACK_TICKET_UUID" -> ticketUuid.toString))

      val (ref, a) = actor()
      ref ! event

      verify(recordingws).updateCallbackTicket(ticketUuid.toString, CallbackTicketPatch(None, None, Some(callid)))
    }
  }
}
