package stats

import akka.testkit.{TestProbe, TestActorRef}
import akkatest.TestKitSpec
import org.mockito.Mockito._
import org.scalatest.mock.MockitoSugar
import org.scalatestplus.play.OneAppPerSuite
import services.XucStatsEventBus
import services.XucStatsEventBus.{StatUpdate, Stat, AggregatedStatEvent}
import stats.Statistic.RequestStat
import stats.StatsAggregator.{SubscribeToBus, Flush}
import xivo.models.XivoObject.{ObjectType, ObjectDefinition}

class StatsAggregatorSpec extends TestKitSpec("StatsAggregatorSpec")
  with OneAppPerSuite
  with MockitoSugar {

  class Helper() {
    val statsBus = mock[XucStatsEventBus]

    def actor() = {
      val sa = TestActorRef(new StatsAggregator(statsBus))
      (sa, sa.underlyingActor)
    }
  }

  override def afterAll() { system.shutdown() }

  "StatsAggregator" should {

    "publish statistic when received following its creation" in new Helper() {
      val (ref, _) = actor()
      val objDef = ObjectDefinition(ObjectType.Queue, Some(11))

      val statUpdate = StatUpdate(objDef, List(Stat("TotalTestCounter", 2.0), Stat("EWT", 2)))
      ref ! statUpdate

      verify(statsBus).publish(AggregatedStatEvent(objDef, List[Stat](Stat("TotalTestCounter", 2.0), Stat("EWT", 2))))
    }

    "subscribe to the stats bus upon reception of the SuscribeToBus message" in new Helper() {
      val (ref, _) = actor()

      ref ! SubscribeToBus

      verify(statsBus).subscribe(ref, ObjectDefinition(ObjectType.Queue))

    }

    "aggregate statistic when received following other statistics" in new Helper() {
      val (ref, _) = actor()
      val objDef1 = ObjectDefinition(ObjectType.Queue, Some(11))
      val objDef2 = ObjectDefinition(ObjectType.Queue, Some(22))

      ref ! StatUpdate(objDef1, List(Stat("TotalTestCounter", 2.0)))

      verify(statsBus).publish(AggregatedStatEvent(objDef1, List[Stat](Stat("TotalTestCounter", 2.0))))
      verifyNoMoreInteractions(statsBus)

      ref ! StatUpdate(objDef1, List(Stat("TotalTestCounter", 3.0), Stat("ShellTestCounter", 3.0)))
      ref ! StatUpdate(objDef2, List(Stat("TotalTestSecondCounter", 5.0)))
      ref ! StatUpdate(objDef1, List(Stat("TotalTestSecondCounter", 2.0)))
      ref ! Flush
      verify(statsBus).publish(AggregatedStatEvent(objDef1,
        List(
          Stat("TotalTestCounter", 3.0),
          Stat("ShellTestCounter", 3.0),
          Stat("TotalTestSecondCounter", 2.0)
        ))
      )
      verify(statsBus).publish(AggregatedStatEvent(objDef2,
        List(
          Stat("TotalTestSecondCounter", 5.0)
        ))
      )
    }

    "replace old values in aggregated statistics" in new Helper() {
      val (ref, _) = actor()
      val objDef1 = ObjectDefinition(ObjectType.Queue, Some(11))
      val objDef2 = ObjectDefinition(ObjectType.Queue, Some(22))

      ref ! StatUpdate(objDef1, List(Stat("TotalTestCounter", 2.0)))
      verify(statsBus).publish(AggregatedStatEvent(objDef1, List[Stat](Stat("TotalTestCounter", 2.0))))
      verifyNoMoreInteractions(statsBus)

      ref ! StatUpdate(objDef1, List(
        Stat("TotalTestCounter", 3.0),
        Stat("ShellTestCounter", 3.0),
        Stat("TotalTestSecondCounter", 2.0))
      )
      ref ! StatUpdate(objDef2, List(Stat("TotalTestSecondCounter", 5.0)))
      ref ! StatUpdate(objDef2, List(Stat("TotalTestSecondCounter", 7.0)))
      ref ! StatUpdate(objDef1, List(Stat("TotalTestSecondCounter", 8.0)))

      ref ! Flush
      verify(statsBus).publish(AggregatedStatEvent(objDef1,
        List(
          Stat("TotalTestCounter", 3.0),
          Stat("ShellTestCounter", 3.0),
          Stat("TotalTestSecondCounter", 8.0)
        ))
      )
      verify(statsBus).publish(AggregatedStatEvent(objDef2,
        List(
          Stat("TotalTestSecondCounter", 7.0)
        ))
      )
    }

    "publish statistic when received after the aggregation period" in new Helper() {
      val (ref, _) = actor()
      val objDef = ObjectDefinition(ObjectType.Queue, Some(11))

      ref ! StatUpdate(objDef, List(Stat("TotalTestCounter", 2.0)))
      verify(statsBus).publish(AggregatedStatEvent(objDef, List[Stat](Stat("TotalTestCounter", 2.0))))
      verifyNoMoreInteractions(statsBus)

      ref ! StatUpdate(objDef, List(Stat("TotalTestCounter", 3.0)))
      ref ! StatUpdate(objDef, List(Stat("TotalTestSecondCounter", 5.0)))
      ref ! Flush
      verify(statsBus).publish(AggregatedStatEvent(objDef,
        List(
          Stat("TotalTestCounter", 3.0),
          Stat("TotalTestSecondCounter", 5.0)
        )
      ))

      ref ! Flush
      ref ! StatUpdate(objDef, List(Stat("TotalTestThirdCounter", 30.0)))
      verify(statsBus).publish(AggregatedStatEvent(objDef, List[Stat](Stat("TotalTestThirdCounter", 30.0))))
    }

  }
}