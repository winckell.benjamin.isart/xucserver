package stats

import akkatest.TestKitSpec
import akka.testkit.TestActorRef
import akka.testkit.TestProbe
import us.theatr.akka.quartz.AddCronSchedule
import services.CronScheduler
import play.api.test.Helpers.running
import play.api.test.FakeApplication

class QueueStatManagerSpec extends TestKitSpec("QueueStatManagerSpec") {
  import stats.Statistic._

  class Helper {
    val queueId = 13
    val cronScheduler = TestProbe()

    trait testQuartzScheduler extends CronScheduler {
      override val scheduler = cronScheduler.ref
    }

    def actor() = {
      val a = TestActorRef(new QueueStatManager(queueId) with testQuartzScheduler)
      (a, a.underlyingActor)
    }
  }

  "Queue Stat Manager" should {
    "self register to quartz scheduler" in new Helper {
      val fApp = FakeApplication(additionalConfiguration = Map("xucstats.resetSchedule" -> "0 32 0 * * ?"))
      running(fApp) {
        val (ref, _) = actor()

        val cronSchedule = AddCronSchedule(ref, "0 32 0 * * ?", ResetStat)

        cronScheduler.expectMsg(cronSchedule)
      }
    }
  }
}