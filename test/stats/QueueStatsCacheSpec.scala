package stats

import akka.actor.Props
import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import org.mockito.Mockito._
import org.scalatest.mock.MockitoSugar
import services.XucStatsEventBus
import services.XucStatsEventBus.{AggregatedStatEvent, Stat, StatUpdate}
import stats.Statistic.RequestStat
import xivo.models.XivoObject.ObjectType._
import xivo.models.XivoObject.{ObjectDefinition, ObjectType}

class QueueStatsCacheSpec extends TestKitSpec("QueueStatsCacheSpec") with MockitoSugar {


  class Helper() {
    val statsBus = mock[XucStatsEventBus]

    def actor() = {
      val sa = TestActorRef(Props(new QueueStatCache(statsBus)))
      (sa, sa.underlyingActor)
    }
  }

  "a queue stat cache" should {
    "Subscribe to wsbus at startup and send start message to router on use router message"  in new Helper {
      val (ref,_) = actor()

      verify(statsBus).subscribe(ref,ObjectDefinition(Queue))

    }

    "send received statistics on request" in new Helper {
      val (ref, _) = actor()
      val aQueue = ObjectDefinition(ObjectType.Queue, Some(11))
      val qStat1 = Stat("TotalTestCounter", 2.0)
      val qStat2 = Stat("Percent", 82.5)

      val requester = TestProbe()

      ref ! AggregatedStatEvent(aQueue, List(qStat1))
      ref ! AggregatedStatEvent(aQueue, List(qStat2))

      ref ! RequestStat(requester.ref, aQueue)

      requester.expectMsg(AggregatedStatEvent(aQueue,List(qStat1, qStat2)))

    }
  }

}
