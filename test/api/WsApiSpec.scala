package api

import org.scalatest.Tag
import org.scalatestplus.play._
import play.api.libs.json.Json
import play.api.libs.ws.{WS, WSRequestHolder}
import play.api.test.Helpers._
import play.api.test._

import scala.concurrent.duration.DurationInt

object WSApiSpecTest extends Tag("xuc.tags.WSApiSpecTest")

// RUN with  wsapi:testOnly *WsApiSpec*

class WsApiSpec extends PlaySpec with OneServerPerSuite {
  val xivohost="xivo-integration"

  val xucRespApiTimeout = 25 seconds

  implicit override lazy val app: FakeApplication =
    FakeApplication(
      additionalConfiguration = Map("xivocti.host"->xivohost,
                                    "XivoWs.host"->xivohost,
                                    "xucami.enabled"->false,
                                    "XivoWs.enabled" -> "true",
        "logger.root"->"OFF",
        "logger.application"->"OFF",
        "logger.play"->"OFF",
        "db.default.url" -> s"""jdbc:postgresql://$xivohost/asterisk?loginTimeout=5&socketTimeout=5&ApplicationName="xuc""""

  )
    )

  def apiUrl(method: String, username: String): String = s"http://localhost:$port/xuc/api/1.0/$method/avencall.com/$username/"

  def globalApiUrl(method: String): String = s"http://localhost:$port/xuc/api/1.0/$method/"

  "Api" should {
    "be able to connect a user" taggedAs(WSApiSpecTest) ignore {
      Thread.sleep(5000)
      val username = "bruce"
      val password = "0000"

      val holder: WSRequestHolder = WS.url(apiUrl("connect", username)).withHeaders(("Content-Type", "application/json"))

      val data = Json.obj(
        "password" -> password)


      val rsp = await(holder.post(data))(xucRespApiTimeout).status
      rsp mustBe (OK)
    }
    "get error on connecting an unknown user" taggedAs(WSApiSpecTest) in {
      Thread.sleep(10000)
      val username = "unknown"
      val password = "pwd"

      val holder: WSRequestHolder = WS.url(apiUrl("connect", username)).withHeaders(("Content-Type", "application/json"))

      val data = Json.obj(
        "password" -> password)

      await(holder.post(data))(xucRespApiTimeout).status mustBe(INTERNAL_SERVER_ERROR)

    }
    "toggle pause an agent in a number" taggedAs(WSApiSpecTest) in {
      val number="1000"
      val holder: WSRequestHolder = WS.url(globalApiUrl("togglePause")).withHeaders(("Content-Type", "application/json"))
      val data = Json.obj(
        "phoneNumber" -> number)

      await(holder.post(data))(xucRespApiTimeout).status mustBe(OK)
    }

    "be able to dnd on using a diconnected user" taggedAs(WSApiSpecTest) in {
      Thread.sleep(5000)
      val username = "bruce"
      val password = "0000"

      val holder: WSRequestHolder = WS.url(apiUrl("dnd", username)).withHeaders(("Content-Type", "application/json"))

      val data = Json.obj(
        "state" -> true)

      await(holder.post(data))(xucRespApiTimeout).status mustBe(OK)
    }
    "be able to dnd off" taggedAs(WSApiSpecTest) in {
      val username = "bruce"
      val password = "0000"

      val holder: WSRequestHolder = WS.url(apiUrl("dnd", username)).withHeaders(("Content-Type", "application/json"))

      val data = Json.obj(
        "state" -> false)

      await(holder.post(data))(xucRespApiTimeout).status mustBe(OK)

    }
    "be able to inconditionnal forward a user" taggedAs(WSApiSpecTest) in {
      val username = "bruce"

      val holder: WSRequestHolder = WS.url(apiUrl("uncForward", username)).withHeaders(("Content-Type", "application/json"))

      val data = Json.obj(
        "state" -> true,
        "destination" -> "1102")

      await(holder.post(data))(xucRespApiTimeout).status mustBe(OK)

    }
    "be able to remove inconditionnal forward a user" taggedAs(WSApiSpecTest) in {
      val username = "bruce"

      val holder: WSRequestHolder = WS.url(apiUrl("uncForward", username)).withHeaders(("Content-Type", "application/json"))

      val data = Json.obj(
        "state" -> false,
        "destination" -> "1102")

      await(holder.post(data))(xucRespApiTimeout).status mustBe(OK)

    }
    "be able to forward on no answer a user" taggedAs(WSApiSpecTest) in {
      val username = "bruce"

      val holder: WSRequestHolder = WS.url(apiUrl("naForward", username)).withHeaders(("Content-Type", "application/json"))

      val data = Json.obj(
        "state" -> false,
        "destination" -> "8812")

      await(holder.post(data))(xucRespApiTimeout).status mustBe(OK)

    }
    "be able to forward on busy a user" taggedAs(WSApiSpecTest)  in {
      val username = "bruce"

      val holder: WSRequestHolder = WS.url(apiUrl("busyForward", username)).withHeaders(("Content-Type", "application/json"))

      val data = Json.obj(
        "state" -> true,
        "destination" -> "5546")

      await(holder.post(data))(xucRespApiTimeout).status mustBe(OK)

    }
    "be able to dial" taggedAs(WSApiSpecTest) in {
      val username = "bruce"

      val holder: WSRequestHolder = WS.url(apiUrl("dial", username)).withHeaders(("Content-Type", "application/json"))

      val data = Json.obj(
        "number" -> "1102")

      await(holder.post(data))(xucRespApiTimeout).status mustBe(OK)
    }

  }

}
